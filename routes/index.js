exports.article = require('./article');
exports.user = require('./user');

exports.index = function(req, res, next) {
  req.models.Article.find({published: true}, null, {sort: {_id:-1}}, function(error, articles){
    if (error) return next(error);
    res.render('index', { articles: articles});
  });
};

exports.mobile = function(req, res, next){
  res.render('mobile');
};

exports.contact = function(req, res){
    res.render('contact');
};

exports.profile = function(req, res, next) {
  res.render('profile');
};

exports.gallery = function(req, res, next) {
  res.render('gallery');
};

exports.webapps = function(req, res, next) {
  res.render('webapps');
};

exports.blog = function(req, res, next){
  req.models.Article.find({published: true}, null, {sort: {_id:-1}}, function(error, articles){
    if (error) return next(error);
    res.render('blog', { articles: articles});
  });
};

exports.uvaOnlineJudge = function(req, res, next) {
  res.render('uva-online-judge');
};

exports.portfolio = function(req, res, next) {
  res.render('portfolio');
};

exports.landscape = function(req, res, next) {
  res.render('landscape');
};

exports.cosmos = function(req, res, next) {
  res.render('cosmos');
};

exports.insect = function(req, res, next) {
  res.render('insect');
};

exports.evolution = function(req, res, next) {
  res.render('evolution');
};

exports.technology = function(req, res, next) {
  res.render('technology');
};

exports.jigsawPuzzle = function(req, res, next) {
  res.render('jigsaw-puzzle');
};

exports.dashboard = function(req, res, next) {
  res.render('dashboard');
};