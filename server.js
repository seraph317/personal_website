var TWITTER_CONSUMER_KEY = process.env.TWITTER_CONSUMER_KEY;
var TWITTER_CONSUMER_SECRET = process.env.TWITTER_CONSUMER_SECRET;

var express = require('express');
var nodemailer = require("nodemailer");
var async = require('async');
var socketio = require('socket.io');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');
var models = require('./models');
mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://@localhost:27017/blog');
var everyauth = require('everyauth');

var session = require('express-session');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

everyauth.debug = true;
everyauth.twitter
  .consumerKey('DfqfiIkBOSSWCMl5mSghg3X0V')
  .consumerSecret('qreLKHSKu1EfGeloxVTYKcb2aA9GL4tDrdEXfSaobL6Y0NeDT8')
  .findOrCreateUser(function(session, accessToken, accessTokenSecret, twitterUserMetadata) {
    var promise = this.Promise();
    process.nextTick(function(){
      if (twitterUserMetadata.screen_name === "seraph317") {
        session.user = twitterUserMetadata;
        session.admin = true;
      }
      promise.fulfill(twitterUserMetadata);
    });
    return promise;
  })
  .redirectPath('/admin');
  
  everyauth.everymodule.handleLogout(routes.user.logout);

everyauth.everymodule.findUserById( function (user, callback) {
  callback(user);
});

var app = express();
app.locals.appTitle = "Yung-Shun Chang's Personal Website";

app.use(function(req, res, next) {
  if (!models.Article || ! models.User) return next(new Error("No models."));
  req.models = models;
  return next();
});

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser('3CCC4ACD-6ED1-4844-9217-82131BDCB239'));
app.use(session({
  secret: '2C44774A-D649-4D44-9535-46E296EF984F',
  proxy: true,
  resave: true,
  saveUninitialized: true
}));
app.use(everyauth.middleware());
app.use(methodOverride());
app.use(require('stylus').middleware(__dirname + '/client'));
app.use(express.static(path.join(__dirname, 'client')));

app.use(function(req, res, next) {
  if (req.session && req.session.admin)
    res.locals.admin = true;
  next();
});

var authorize = function(req, res, next) {
  if (req.session && req.session.admin)
    return next();
  else
    return res.sendStatus(401);
};

if ('development' == app.get('env')) {
  app.use(errorHandler());
}

app.get('/', routes.index);
app.get('/mobile', routes.mobile);
app.get('/contact', routes.contact);
app.get('/profile', routes.profile);
app.get('/blog', routes.blog);
app.get('/uva-online-judge', routes.uvaOnlineJudge);
app.get('/portfolio', routes.portfolio);
app.get('/landscape', routes.landscape);
app.get('/cosmos', routes.cosmos);
app.get('/insect', routes.insect);
app.get('/evolution', routes.evolution);
app.get('/technology', routes.technology);
app.get('/gallery', routes.gallery);
app.get('/webapps', routes.webapps);
app.get('/jigsaw-puzzle', routes.jigsawPuzzle);
app.get('/dashboard', routes.dashboard);
app.get('/login', routes.user.login);
app.post('/login', routes.user.authenticate);
app.get('/logout', routes.user.logout);
app.get('/admin', authorize, routes.article.admin);
app.get('/post', authorize, routes.article.post);
app.post('/post', authorize, routes.article.postArticle);
app.get('/articles/:slug', routes.article.show);

app.post('/contact',function(req,res){
  var smtpTransport = nodemailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
      user: "seraph317@gmail.com",
      pass: "Seraph051379"
    }
  });

  var mailOptions={
    from: req.body.name + ' &lt;' + req.body.email + '&gt;',
    to : "seraph317@gmail.com",
    subject : req.body.subject,
    text : req.body.message
  };

  console.log(mailOptions);
  smtpTransport.sendMail(mailOptions, function(error, response){
    if (error) {
      res.render('contact', { title: 'Contact', msg: 'Error occured, message not sent.', err: true, page: 'contact' });
    } else {
      res.render('contact', { title: 'Contact', msg: 'Message sent! Thank you.', err: false, page: 'contact' });
    }
  });
});

app.all('/api', authorize);
app.get('/api/articles', routes.article.list);
app.post('/api/articles', routes.article.add);
app.put('/api/articles/:id', routes.article.edit);
app.delete('/api/articles/:id', routes.article.del);

app.all('*', function(req, res) {
  res.sendStatus(404);
});

var server = http.createServer(app);
var boot = function () {
  server.listen(app.get('port'), function(){
    console.info('Express server listening on port ' + app.get('port'));
  });
};
var shutdown = function() {
  server.close();
};
if (require.main === module) {
  boot();
} else {
  console.info('Running app as a module');
  exports.boot = boot;
  exports.shutdown = shutdown;
  exports.port = app.get('port');
}