$(document).ready(function()
{
    $( "#dialogZoom" ).dialog(
		{
			resizable: false,
			autoOpen : false,
			modal: true,
			width: "auto",
			show : 'scale',
			hide : 'scale'
		});
	$('img.img-thumb').on('click', function(event) 
		{
		    var target = $(event.target);
			var largeImagePath = target.attr('src');
			$('#dialogZoom').html('<img src="' + largeImagePath + '">').dialog('open');
			return false;
		});
});