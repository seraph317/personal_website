$(document).ready(function()
{
	$('#dashboard').mouseenter( function(event) {
		var targetHover = $(this);
		$( targetHover ).animate({
		    height: "595px", width: "665px", opacity: "1"
        }, 1, function() {});
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    height: "591px", width: "661px", opacity: "0.6"
		}, 1, function() {});
    });
    
    $('#jigsaw-puzzle').mouseenter( function(event) {
		var targetHover = $(this);
		$( targetHover ).animate({
		    height: "595px", width: "665px", opacity: "1"
        }, 1, function() {});
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    height: "591px", width: "661px", opacity: "0.6"
		}, 1, function() {});
    });
});