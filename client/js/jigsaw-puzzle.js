var chosenImg;
var imgUrl = '';
var rows = 5;
var cols = 4;

$(document).ready(function() {
  $('.jigsaw-puzzle-img-thumb').on('click', function() {
      chosenImg = $(this).prop('alt');
      switch(chosenImg) {
          case 'MonaLisa':
			imgUrl = '/img/MonaLisa.jpg';
		  break;
		  case 'Napoleon':
			imgUrl = '/img/Napoleon.jpg';
		  break;
		  case 'Spring':
			imgUrl = '/img/Spring.jpg';
		  break;
      }
      var sliceStr = createSlices(true);
      $('#puzzleContainer').html(sliceStr);
  });

  $('#jigsaw-puzzle-start').on('click', function() {
      var divs = $('#puzzleContainer div');
      var allDivs = shuffle(divs);
      $('#pieceBox').empty();
      allDivs.each(function() {
          var leftDistance = Math.floor((Math.random()*280)+0) + 'px';
          var topDistance = Math.floor((Math.random()*280)+0) + 'px';
          $(this)
              .addClass('imgDraggable')
              .css({
                  position: 'absolute',
                  left: leftDistance,
                  top: topDistance
              });
          $('#pieceBox').append($(this));
      });
      var sliceStr = createSlices(false);
      $('#puzzleContainer').html(sliceStr);
      
      $(this).hide();
      $('#jigsaw-puzzle-reset').show();
      
      addPuzzleEvents();
  });
  
  $('#jigsaw-puzzle-reset').on('click', function() {
     var sliceStr = createSlices(true);
     $('#puzzleContainer').html(sliceStr);
     $('#pieceBox').empty();
     $('#jigsaw-puzzle-message').empty().hide();
     $(this).hide();
     $('#jigsaw-puzzle-start').show();
  });
});

function createSlices(useImage) {
    var sliceArr = [];
    for(var i = 0, top = 0, count = 0; i < rows; i++, top -= 100) {
        for(var j = 0, left = 0; j < cols; j++, left -= 100, count++) {
            if (useImage) {
                sliceArr.push('<div style="background-position: ' + left + 'px ' + top + 'px; background-image: url('+ imgUrl +');" class = "jigsaw-puzzle-img" data-sequence="' + count + '">');
            } else {
                sliceArr.push('<div style="background-image: none;" class="jigsaw-puzzle-img imgDroppable">');
            }
            sliceArr.push('</div>');
        }
    }
    return sliceArr.join('');
}
         
function shuffle(o) {
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function checkIfPuzzleComplete() {
    if ($('#puzzleContainer div.pieceDropped').length != 20) {
        return false;
    }
    for (var i = 0; i < 20; i++) {
        var puzzlePiece = $('#puzzleContainer div.pieceDropped:eq('+i+')');
        var sequence = parseInt(puzzlePiece.data('sequence'), 10);
        if (i != sequence) {
            switch(chosenImg) {
                case 'MonaLisa':
                  $('#jigsaw-puzzle-message').text('Nope! You made Mona Lisa sad :(').show();
                break;
                case 'Napoleon':
                  $('#jigsaw-puzzle-message').text('Nope! You made Napoleon cry :(').show();
                break;
                case 'Spring':
                  $('#jigsaw-puzzle-message').text('Nope! You made the girl cry :(').show();
                break;
            }
            return false;
        }
    }
    switch(chosenImg) {
        case 'MonaLisa':
          $('#jigsaw-puzzle-message').text('YaY! Mona Lisa is smiling now :)').show();
        break;
        case 'Napoleon':
          $('#jigsaw-puzzle-message').text('YaY! Napoleon crossing the alps :)').show();
        break;
        case 'Spring':
          $('#jigsaw-puzzle-message').text('YaY! Pursuing the girl successfully :)').show();
        break;
    }
    return true;
}

function addPuzzleEvents() {
    $('.imgDraggable').draggable({
       revert: 'invalid',
       zIndex: 100,
       start: function(event, ui) {
           var $this = $(this);
           if($this.hasClass('pieceDropped')) {
               $this.removeClass('pieceDropped');
               ($this.parent()).removeClass('piecePresent');
           }
       }
    });
    
    $('.imgDroppable').droppable({
        hoverClass: "ui-state-highlight",
        accept: function(draggable) {
            return !$(this).hasClass('piecePresent');
        },
        drop: function(event, ui) {
            var draggable = ui.draggable;
            var droppedOn = $(this);
            droppedOn.addClass('piecePresent');
            $(draggable).detach().addClass('pieceDropped imgDraggable').css({
                top: 0,
                left: 0,
                position: 'relative'
            }).appendTo(droppedOn);
            
            checkIfPuzzleComplete();
        }
    });
}
