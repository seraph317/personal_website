$(window).bind('scroll', function () {
    var getMenu = document.getElementById("menu");
    var menuTop = getMenu.offsetTop;
    if ($(window).scrollTop() > menuTop) {
        $('#menu').addClass('navbar-fixed-top');
        $('ul.nav.navbar-nav').addClass('navbar-right');
        $('.navbar-brand').empty();
        $('.navbar-brand').text("Yung Shun Chang's Personal Website");
    } else {
        $('#menu').removeClass('navbar-fixed-top');
        $('ul.nav.navbar-nav').removeClass('navbar-right');
        $('.navbar-brand').empty();
        $('.navbar-brand').text("Welcome");
    }
});