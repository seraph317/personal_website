$(document).ready(function() {
    var $window = $(window);
    var lastScrollTopLeftTop = 0;
    var deltaLeftTop = 5;
    var lastScrollTopRightTop = 0;
    var deltaRightTop = 5;
    var lastScrollTopLeftMid = 0;
    var deltaLeftMid = 5;
    var lastScrollTopRightMid = 0;
    var deltaRightMid = 5;
    var lastScrollTopLeftBottom = 0;
    var deltaLeftBottom = 5;
    var lastScrollTopRightBottom = 0;
    var deltaRightBottom = 5;
    var $slideLeftTop = $('#slide-left-top');
    var $slideRightTop = $('#slide-right-top');
    var $slideLeftMid = $('#slide-left-mid');
    var $slideRightMid = $('#slide-right-mid');
    var $slideLeftBottom = $('#slide-left-bottom');
    var $slideRightBottom = $('#slide-right-bottom');

    $window.on('scroll', function() {
        var currentScrollTopLeftTop = $(this).scrollTop();
        if(Math.abs(lastScrollTopLeftTop - currentScrollTopLeftTop) <= deltaLeftTop)
          return;
        if (currentScrollTopLeftTop > lastScrollTopLeftTop){
            if ( $(window).scrollTop() > 1800 && $(window).scrollTop() < 4200) {
                $slideLeftTop.animate({ 'left': '-20px' }, 500);
            } else {
                $slideLeftTop.stop(true).animate({ 'left': '-2000px' }, 500);
            }
        } else {
            if ( $(window).scrollTop() < 3400 && $(window).scrollTop() > 2000) {
                $slideLeftTop.animate({ 'left': '-20px' }, 500);
            } else {
                $slideLeftTop.stop(true).animate({ 'left': '-2000px' }, 500);
            }
        }
        lastScrollTopLeftTop = currentScrollTopLeftTop;
    });
    
    $window.on('scroll', function() {
        var currentScrollTopRightTop = $(this).scrollTop();
        if(Math.abs(lastScrollTopRightTop - currentScrollTopRightTop) <= deltaRightTop)
          return;
        if (currentScrollTopRightTop > lastScrollTopRightTop){
            if ( $(window).scrollTop() > 1800 && $(window).scrollTop() < 4200) {
                $slideRightTop.animate({ 'right': '0px' }, 500);
            } else {
                $slideRightTop.stop(true).animate({ 'right': '-2000px' }, 500);
            }
        } else {
            if ( $(window).scrollTop() < 3400 && $(window).scrollTop() > 2000) {
                $slideRightTop.animate({ 'right': '0px' }, 500);
            } else {
                $slideRightTop.stop(true).animate({ 'right': '-2000px' }, 500);
            }
        }
        lastScrollTopRightTop = currentScrollTopRightTop;
    });

        $window.on('scroll', function() {
        var currentScrollTopLeftMid = $(this).scrollTop();
        if(Math.abs(lastScrollTopLeftMid - currentScrollTopLeftMid) <= deltaLeftMid)
          return;
        if (currentScrollTopLeftMid > lastScrollTopLeftMid){
            if ( $(window).scrollTop() > 2100 && $(window).scrollTop() < 3900) {
                $slideLeftMid.animate({ 'left': '-20px' }, 500);
            } else {
                $slideLeftMid.stop(true).animate({ 'left': '-2000px' }, 500);
            }
        } else {
            if ( $(window).scrollTop() < 3700 && $(window).scrollTop() > 1700) {
                $slideLeftMid.animate({ 'left': '-20px' }, 500);
            } else {
                $slideLeftMid.stop(true).animate({ 'left': '-2000px' }, 500);
            }
        }
        lastScrollTopLeftMid = currentScrollTopLeftMid;
    });
    
    $window.on('scroll', function() {
        var currentScrollTopRightMid = $(this).scrollTop();
        if(Math.abs(lastScrollTopRightMid - currentScrollTopRightMid) <= deltaRightMid)
          return;
        if (currentScrollTopRightMid > lastScrollTopRightMid){
            if ( $(window).scrollTop() > 2100 && $(window).scrollTop() < 3900) {
                $slideRightMid.animate({ 'right': '0px' }, 500);
            } else {
                $slideRightMid.stop(true).animate({ 'right': '-2000px' }, 500);
            }
        } else {
            if ( $(window).scrollTop() < 3700 && $(window).scrollTop() > 1700) {
                $slideRightMid.animate({ 'right': '0px' }, 500);
            } else {
                $slideRightMid.stop(true).animate({ 'right': '-2000px' }, 500);
            }
        }
        lastScrollTopRightMid = currentScrollTopRightMid;
    });

    $window.on('scroll', function() {
        var currentScrollTopLeftBottom = $(this).scrollTop();
        if(Math.abs(lastScrollTopLeftBottom - currentScrollTopLeftBottom) <= deltaLeftBottom)
          return;
        if (currentScrollTopLeftBottom > lastScrollTopLeftBottom){
            if ( $(window).scrollTop() > 2400 && $(window).scrollTop() < 3600) {
                $slideLeftBottom.animate({ 'left': '-20px' }, 500);
            } else {
                $slideLeftBottom.stop(true).animate({ 'left': '-2000px' }, 500);
            }
        } else {
            if ( $(window).scrollTop() < 4000 && $(window).scrollTop() > 1400) {
                $slideLeftBottom.animate({ 'left': '-20px' }, 500);
            } else {
                $slideLeftBottom.stop(true).animate({ 'left': '-2000px' }, 500);
            }
        }
        lastScrollTopLeftBottom = currentScrollTopLeftBottom;
    });
    
    $window.on('scroll', function() {
        var currentScrollTopRightBottom = $(this).scrollTop();
        if(Math.abs(lastScrollTopRightBottom - currentScrollTopRightBottom) <= deltaRightBottom)
          return;
        if (currentScrollTopRightBottom > lastScrollTopRightBottom){
            if ( $(window).scrollTop() > 2400 && $(window).scrollTop() < 3600) {
                $slideRightBottom.animate({ 'right': '0px' }, 500);
            } else {
                $slideRightBottom.stop(true).animate({ 'right': '-2000px' }, 500);
            }
        } else {
            if ( $(window).scrollTop() < 4000 && $(window).scrollTop() > 1400) {
                $slideRightBottom.animate({ 'right': '0px' }, 500);
            } else {
                $slideRightBottom.stop(true).animate({ 'right': '-2000px' }, 500);
            }
        }
        lastScrollTopRightBottom = currentScrollTopRightBottom;
    });
});