var timeDisplay;
var lat = '37.774798';
var lng = '-122.422837';

function initClock() {
    timeDisplay = document.createTextNode ( "" );
    document.getElementById("dashboard-clock").appendChild ( timeDisplay );
}

function updateClock() {
    var currentTime = new Date ( );
	var currentHours = currentTime.getHours ( );
	var currentMinutes = currentTime.getMinutes ( );
	var currentSeconds = currentTime.getSeconds ( );
	
	currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
	currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
		    
	var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";
		    
	currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;
		    
	currentHours = ( currentHours == 0 ) ? 12 : currentHours;
		    
	var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
		    
	document.getElementById("dashboard-clock").firstChild.nodeValue = currentTimeString;
		    
	setTimeout(updateClock, 1000);
}

initClock();
updateClock();

$(document).ready(function()
{
    var dashboard = {
        init : function() {
            this.initPortlets();
            this.setupWeather();
            this.displayWeather();
            this.initFlickr();
            this.initReddit();
        },
        initPortlets : function() {
            $(".dashboard-column").sortable(
			{
				connectWith: ".dashboard-column",
				handle: ".portlet-header",
				cancel: ".portlet-toggle",
				placeholder: "portlet-placeholder ui-corner-all"
			});

			$(".portlet")
				.addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".portlet-header")
				.addClass("ui-widget-header ui-corner-all")
				.prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

			$( ".portlet-toggle" ).click(function() 
			{
				var icon = $( this );
				icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
				icon.closest(".portlet").find(".portlet-content").toggle('fast');
			});
			$('#loadingWeather').hide();
			$('#weatherInfo').hide();
        },
        setupWeather : function() {
			var cities = ['San Francisco, USA', 'New York, USA', 'Taipei, ROC'];
			var strCity;
			$(cities).each(function(i, item)
			{
				strCity+= '<option value="' + item + '">' + item + '</option>';
			});
			$('#selCity').html(strCity);

			$('#selCity').change(function()
			{
				var selection = $(this).val();
				switch(selection) {
					case 'San Francisco, USA':
						lat = 37.774798;
						lng = -122.422837;
					break;
					case 'New York, USA':
						lat = 40.710600;
						lng = -74.004400;
					break;
					case 'Tapei, ROC':
						lat = 25.044601;
						lng = 121.559936;
					break;
				}
				dashboard.displayWeather();
			});
		},
		displayWeather : function() {
			$('#loadingWeather').show();
			$('#weatherInfo').hide();
			var apiURL = 'https://api.forecast.io/forecast/aa555b707a65fb933f9355afacb277b6/' + lat + ',' + lng;
			$.ajax(
			{
			  url: apiURL,
			  dataType: "jsonp",
			  jsonp: 'callback',
			  success: function(weatherData)
			  {
				var x = {data : weatherData};
				console.log(x);
			    $('#dashboard-summary').html(' ' + weatherData.currently.summary);
				$('#dashboard-temperature').html(' ' + weatherData.currently.temperature + ' degree Fahrenheit');
				$('#dashboard-precipProbability').html(' ' + weatherData.currently.precipProbability);
				$('#dashboard-humidity').html(' ' + weatherData.currently.humidity);
				$('#dashboard-windSpeed').html(' ' + weatherData.currently.windSpeed + ' mph');

				var googleUrl = 'https://www.google.com/maps?q=' + lat + ',' + lng;
				var googleLink = ' <a href="' + googleUrl + '" target="_blank">View on Google maps</a>';

				$('#dashboard-location').html(' (' + lat + ', '+ lng + ')' + googleLink);
				
				$('#weatherInfo').show();
				$('#loadingWeather').hide();
			  },
			  error: function (a,b,c)
			  {
				console.log('Error getting weather.');
			  }
			});
		},
		initFlickr : function()
		{
			$.getJSON('https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?',
			{
			   tags: 'dog', 
			   format: 'json'
			},
			function(data) 
			{
				var str = '';
				var x = {data : data};
				console.log(x);
				$.each(data.items, function(i,item)
				{
					str+= '<li>';
					str+= '<a class="media" href="javascript:;" data-img="' + item.media.m + '">';
					str+= '<img src="' + item.media.m + '">';
					str+= '</a>';					
					var permaLink = '<a href="' + item.link +'" target="_blank">link</a>';
					str+= '<strong>' + item.title + '</strong>( ' + permaLink + ')<br><br>tags : ' + item.tags;
					str+= '</li>';
				});
				$('#dashboard-flickrPics').html(str);
			});

			$('#dashboard-flickrPics').on('click', 'a.media', function()
			{
				var img = $(this).data('img');
				$('#dashboard-dialog').html('<img src="' + img + '">').dialog({modal : true});
			});
		},
		initReddit : function()
		{
			var apiURL = 'https://www.reddit.com/r/all.json';
			$.ajax(
			{
			  url: apiURL,
			  dataType: "jsonp",
			  jsonp: 'jsonp',
			  success: function(data)
			  {
				var x = {a : data};
				console.log(x);
				$('#dashboard-reddit').html(dashboard.getRedditThreadList(data.data.children));
			  },
			  error: function (a,b,c)
			  {
				alert('Error getting data');
			  }
			});
		},
		getRedditThreadList : function(postListing)
		{
			var strHtml = '<ul>';
			for(var i = 0; i < postListing.length; i++)
			{
				var aPost = postListing[i].data;
				var permalink = 'http://reddit.com' + aPost.permalink;

				strHtml+= '<li>';
				strHtml+= (i+1) + ' - <span>[' + aPost.subreddit + ']</span> <a href="'+aPost.url+'" target="_blank">' + aPost.title + '</a> (score : ' + aPost.score + '| <a class="comments" href="' + permalink + '" target="_blank"> comments : ' + aPost.num_comments + '</a>)';
				strHtml+= '</li>';
			}
			strHtml+= '</ul>';
			return strHtml;
		},
    };
    dashboard.init();
});