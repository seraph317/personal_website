$(document).ready(setup);

function setup() {
    var text = "Hello. Welcome to my work portfolio./I am a front-end engineer, who write most codes in JavaScript, HTML, and CSS./";
        text += "Have experiences building real world web apps./Also proficient in many JavaScript libraries, like jQuery, jQuery UI, and Bootstrap./";
        text += "Other than front-end techniques,/I also learn back-end development skills and use them practically./";
        text += "For example, I built websites with node.js or ruby on rails./My interests mainly lie in making dynamic and interactive user interface./";
        text += "Hope we can have further cooperation!/"; 
    $('#portfolio-roller, #one').addClass('scene');
    letters(100, "#one span, #one br", "#one", text);
}

function letters(initialDelay, selector, element, text) {
  var re = /[-.,!\w]/;     
  for (var i in text) {
    if (text[i].match(re)) {
      var el = document.createElement("span");
      var letter = document.createTextNode(text[i]);
      el.appendChild(letter);
      $(element).append(el);
    }
    else if (text[i] === "/") {
      $(element).append(document.createElement("br"));
    }
    else {
       $(element).append(document.createTextNode(text[i]));
    }
  }
  var delay = initialDelay;
  $(selector).each(function() {
    if ($(this).is('span')) {
      this.style[Modernizr.prefixed('animation')] = "letterAppear 100ms " + delay + "ms forwards";  
      delay += 100;        
    }
    else if ($(this).is('br') && ( ! $(this).is('.skip'))) {
      delay += 1500;
    } 
  });
}