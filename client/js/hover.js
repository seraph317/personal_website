$(document).ready(function()
{
    $('#portfolio-1').tooltip(
	{
		items : '#portfolio-1',
		position: { my: 'left top', at: 'right', collision : 'flip' },
		content : function()
		{
			var strLandscape = '<strong><span class="glyphicon glyphicon-camera"></span> Formosan Landscape</strong><br>';
			    strLandscape += 'A web app introducing the natural wonders of Taiwan. To avoid tons of data causing information overload, the use of <strong>Google Maps API</strong> hides them first, only showing their positions or <strong>icon</strong>s.';
			    strLandscape += "The app also enables users to customize their <strong>filter</strong>s (using <strong>slider</strong> and <strong>spinner</strong>) or to click on the <strong>marker</strong>s directly to get further information (integrate jQuery UI's <strong>tabs</strong> into Google Maps' <strong>info windows</strong>).<br> ";
			    strLandscape += '<strong><span class="glyphicon glyphicon-tag"></span> Key Skills:</strong><br><strong>Google Maps API, jQuery, jQuery UI, HTML, CSS, JavaScript, Node.js</strong>';
			    return strLandscape;
		}
	});
	
	$('#portfolio-2').tooltip(
	{
		items : '#portfolio-2',
		position: { my: 'left top', at: 'right', collision : 'flip' },
		content : function()
		{
			var strCosmos = '<strong><span class="glyphicon glyphicon-star"></span> Cosmos Wanderer</strong><br>';
			    strCosmos += "A trial of creating a <strong>parallax scrolling website</strong> with <strong>skrollr.js</strong>. While scrolling down, you will see many <strong>animation</strong> effects: a fixed-positioned space shuttle, elements fading in and out, the asteroid belt and planets scrolling with different speeds, "; 
			    strCosmos += 'the satellite generating shadow on the planet, one background picture moving left, a curtain comming down, and the background images scrolling one page at a time.<br>';
			    strCosmos += '<strong><span class="glyphicon glyphicon-tag"></span> Key Skills:</strong><br><strong>skrollr.js, jQuery, HTML, CSS, JavaScript, Node.js</strong>';
			    return strCosmos;
		}
	});
	
	$('#portfolio-3').tooltip(
	{
		items : '#portfolio-3',
		position: { my: 'right top', at: 'left', collision : 'flip' },
		content : function()
		{
			var strInsect = '<strong><span class="glyphicon glyphicon-leaf"></span> Insect World</strong><br>';
			    strInsect += 'On this website, I focused on some new CSS tricks, such as <strong>the flexible box layout</strong> and <strong>the :before and :after pseudo-elements</strong>. '; 
			    strInsect += "Furthermore, I also created a Jasny-Bootstrap-like <strong>off-canvas navigation menu</strong> from scratch and wrote a <strong>smooth-scrolling</strong>-to-an-anchor effect in jQuery. ";
			    strInsect += 'To make the site more interactive, adding lots of <strong>tooltips</strong> is not a bad idea.<br>';
			    strInsect += '<strong><span class="glyphicon glyphicon-tag"></span> Key Skills:</strong><br><strong>jQuery, Bootstrap, jQuery UI, HTML, CSS, JavaScript, Node.js</strong>';
			    return strInsect;
		}
	});
	
	$('#portfolio-4').tooltip(
	{
		items : '#portfolio-4',
		position: { my: 'left top', at: 'right', collision : 'flip' },
		content : function()
		{
			var strEvolution = '<strong><span class="glyphicon glyphicon-hourglass"></span> Human Evolution</strong><br>';
			    strEvolution += "Except the personal site, this is my favorite design I've made so far. This site begins with a transparent Bootstrap <strong>navigation bar</strong> integrated in a Bootstrap <strong>jumbotron</strong>. Inside the jumbotron, you "; 
			    strEvolution += 'can see human evolution silhouettes <strong>cycle</strong>d one after another. Then there are three sections with fixed-position backgrounds used to create a <strong>simple parallax scrolling effect</strong>. Next, I inset the Bootstrap <strong>carousel</strong>s into the <strong>tab</strong> panels, saving space ';
			    strEvolution += "from 15 pages to 1 page. What's more, using <strong>Bootstrap carousel slide events</strong> to highlight the annotations dynamically lets users read without difficulty.<br>";
			    strEvolution += '<strong><span class="glyphicon glyphicon-tag"></span> Key Skills:</strong><br><strong>jQuery, Bootstrap, HTML, CSS, JavaScript, Node.js</strong>';
			    return strEvolution;
		}
	});
	
	$('#portfolio-5').tooltip(
	{
		items : '#portfolio-5',
		position: { my: 'left top', at: 'right', collision : 'flip' },
		content : function()
		{
			var strScrivener = '<strong><span class="glyphicon glyphicon-home"></span> Land Scrivener Service</strong><br> ';
			    strScrivener += 'This is the official website I created for Chuan-Min & Mei-Ing Land Scrivener Service Office (or called Liu-Xiang Commercial Real Estate), only using the vanilla CSS and mostly plain JavaScript. There are many UI widgets used on this site, such as '; 
			    strScrivener += '<strong>accordion</strong>, <strong>slider</strong>, <strong>tabs</strong>, <strong>slide advertisement</strong>, and <strong>Google Maps</strong>. But what took me most effort is its backend (with <strong>Ruby on Rails</strong>), the account management system, including <strong>sign up</strong>, <strong>log in</strong>, <strong>log out</strong>, <strong>account activation</strong>, ';
			    strScrivener += '<strong>password reset</strong>, and a <strong>blog</strong> where users can post and delete news.<br>';
			    strScrivener += '<strong><span class="glyphicon glyphicon-tag"></span> Key Skills:</strong><br><strong>JavaScript, HTML, CSS, jQuery, Ruby on Rails</strong>';
			    return strScrivener;
		}
	});
	
	$('#portfolio-6').tooltip(
	{
		items : '#portfolio-6',
		position: { my: 'right top', at: 'left', collision : 'flip' },
		content : function()
		{
			var strTransaction = '<strong><span class="glyphicon glyphicon-map-marker"></span> Real Estate Transaction</strong><br> ';
			    strTransaction += "This web app was built when I participated in a Hackathon. It's a data visualization project maping Taiwan's real estate transaction records with Google Maps. Other than <strong>Google Maps API</strong>, "; 
			    strTransaction += "it also requires the <strong>Ruby Geocoder gem</strong> to transform addresses into coordinates. It is still under construction right now, but coming soon.<br>";
			    strTransaction += '<strong><span class="glyphicon glyphicon-tag"></span> Key Skills:</strong><br><strong>Google Maps API, Geocoding API, jQuery, jQuery UI, HTML, CSS, JavaScript, Ruby on Rails</strong>';
			    return strTransaction;
		}
	});
	
	$('.portfolio').mouseenter( function(event) {
		var targetHover = $(this);
		var targetEnterTitle = $(this).siblings('.portfolio-title');
		targetHover.addClass("portfolio-active");
		targetEnterTitle.animate({top: "260px"}, 1);
		$( ".portfolio-active" ).animate({
		    height: "260px"
        }, 1, function() {
            $(".portfolio").not(".portfolio-active").animate({opacity: "0.5"}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        var targetLeaveTitle = $(this).siblings('.portfolio-title');
        targetLeave.removeClass("portfolio-active");
        targetLeaveTitle.animate({top: "300px"}, 1);
        $(".portfolio").not(".portfolio-active").animate({opacity: "1"}, 1);
        $( targetLeave ).animate({
		    height: "300px"
        }, 1, function() {});
    });
});