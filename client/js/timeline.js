$(function()
{
	objTimeline.init();
});
var objTimeline = 
{
	minYear : 0,
	maxYear : 0,
	currentYear : 0,
	itemsToDisplay : 4,
	maxScrollYear : 0,
	timelineWindowStartYear : 0,
	windowLeft: 0,
	isWindowOpen : false,
	timelineData : 
	[
		{
			year : 2001,
			date : "Aug 24, 2015",
			events : ['Initialize repository']
		},
		{
			year : 2002,
			date : "Aug 31, 2015",
			events : ['Update package.json and install npm packages', 'Set up an HTTP server', 'Set up preliminary routes', 'Set up preliminary views', 'Set up preliminary models', 'Create share buttons']
		},
		{
			year : 2003,
			date : "Sep 1, 2015",
			events : ['Customize navigation bar', 'Smoothly scroll down to next panel when clicking an anchor link', 'Finish sliding panel animations & transitions', 'Create an event timeline using a slider', 'Create a dynamic resume', 'Use the Google Static Maps API']
		},
		{
			year : 2004,
			date : "Sep 2, 2015",
			events : ['Filter tagged images', 'Enable image search', 'Add a gallery carousel', 'Provide a zoom-in function for images', 'Build a jigsaw puzzle game', 'Let users select one image to be used with the puzzle']
		},
		{
			year : 2005,
			date : "Sep 3, 2015",
			events : ['Initialize the portlets', 'Create a weather widget', 'Display posts from the reddit front page', 'Display Flickr photos', 'Create a JavaScript clock', 'Add tooltips for resume']
		},
		{
			year : 2006,
			date : "Sep 4, 2015",
			events : ['Turn off jQuery UI tooltips when taking a tour', 'Improve my profile page', 'Deploy to Heroku', 'Install MongoLab on my Heroku app', 'Change jQuery UI theme', 'Different elements scroll at different speeds']
		},
		{
			year : 2007,
			date : "Sep 5, 2015",
			events : ['Create a parallax scrolling animation']
		},
		{
			year : 2008,
			date : "Sep 6, 2015",
			events : ['Change the background image', 'Improve the exterior appearance', 'Solve the overflow problem', 'Animate background images with fade in/out effect', 'Adjust the timeout setting', 'Improve visual performance of the background cycler']
		},
		{
			year : 2009,
			date : "Sep 7, 2015",
			events : ['Complete the preliminary version of my homepage', 'Create new views and routes', 'Add hover effects to my profile', 'Add information to my timeline']
		},
		{
			year : 2010,
			date : "Sep 9, 2015",
			events : ['Improve website appearance', 'Rename articles as blog', 'Leave menu bar fixed on top when scrolled', 'Add glyphicons to menu']
		},
		{
			year : 2011,
			date : "Sep 10, 2015",
			events : ['Create a typewriter animation', 'Animate text', 'Use jQuery UI with Google Maps API']
		},
		{
		    year : 2012,
			date : "Sep 11, 2015",
			events : ['Set icons and infowindows in the map']
		},
		{
		    year : 2013,
			date : "Sep 12, 2015",
			events : ['Parallax scrolling effect using skrollr.js']
		},
		{
		    year : 2014,
			date : "Sep 15, 2015",
			events : ['Improve portofolio performance', 'Change portofolio layout', 'Add glyphicons to my portofolio', 'Solve a display problem', 'Use CSS flexible boxes', 'Create tab panels']
		},
		{
		    year : 2015,
			date : "Sep 16, 2015",
			events : ['Create new sections']
		},
		{
		    year : 2016,
			date : "Sep 17, 2015",
			events : ['Create a jumbotron', 'Add slide animation effects', 'Use the custom font Akashi']
		},
		{
		    year : 2017,
			date : "Sep 19, 2015",
			events : ['Add background images to my homepage', 'Add data that are displayed aside the map', 'Add contact form to footer']
		},
		{
		    year : 2018,
			date : "Sep 20, 2015",
			events : ['Fix the timeline display issue', 'Upload PNG images and then apply different scroll speeds', 'Add portfolio links to footer']
		},
		{
		    year : 2019,
			date : "Sep 21, 2015",
			events : ['Write content and upload images for the website', 'Create parallax scrolling effects with fixed position background images', 'Use Bootstrap carousel plugin', 'Integrate carousels into tab panels', 'Add page scroll effects', 'Update my portfolio gallery']
		},
		{
		    year : 2020,
			date : "Sep 23, 2015",
			events : ['Use Bootstrap carousel slide events', 'Add content to the parallax scrolling website', 'Use another custom font Michroma', 'Use Jasny Bootstrap off-canvas navigation', 'Add tooltips to my portfolio', 'Improve hover effects of my portfolio']
		},
		{
		    year : 2021,
			date : "Sep 24, 2015",
			events : ['Create interactive UI with tooltips and mouse events', 'Add sharebox to footer']
		},
		{
		    year : 2022,
			date : "Sep 25, 2015",
			events : ['Add more interactive elements to my website', 'Improve layout of tooltips', 'Send email on form submission using Nodemailer', 'Add more animation effects to my homepage', 'Add an icon to header']
		},
		{
		    year : 2023,
			date : "Sep 27, 2015",
			events : ['Create the index page for my web apps', 'Add annotations to web apps', 'Create modals with Bootstrap', 'Add a contact form to the footer of my portfolio', 'Import JSON into MongoDB']
		},
		{
		    year : 2024,
			date : "Sep 28, 2015",
			events : ['Display code with highlight.js', 'Embed PDF document in HTML page', 'Decode HTML entities using JavaScript', 'Get JSON object from Ajax call']
		},
		{
		    year : 2025,
			date : "Sep 29, 2015",
			events : ['Display dynamic data', 'Add pagination to my blog', 'Update my resume']
		},
		{
		    year : 2026,
			date : "Sep 30, 2015",
			events : ['Move JavaScript includes to the end of body tag']
		},
		{
		    year : 2027,
			date : "Oct 1, 2015",
			events : ['Rename app on Heroku']
		},
		{
		    year : 2028,
			date : "Oct 2, 2015",
			events : ['Change hyperlink paths']
		},
	],
	init : function()
	{
		this.createMarkup();
		this.createTimeline();
	},
	createMarkup : function()
	{
	    $('.timeline-container').css({width: (objTimeline.itemsToDisplay * 200 + 2) + 'px'});
		$('#rightOverlay').css({ width: ((objTimeline.itemsToDisplay * 200) - 200) + 'px' });
		this.minYear = this.timelineData[0].year;
		this.maxYear = this.timelineData[0].year;
		var strYearDivs = '';
		for(var i=0; i< this.timelineData.length; i++)
		{
			var singularPlural = 'commits';
			if (this.timelineData[i].events.length == 0 || this.timelineData[i].events.length == 1) {
				singularPlural = 'commit';
			}
			strYearDivs+= '<div class="year">';
			strYearDivs+= '<div style="margin-top: 240px;"><strong>'+ this.timelineData[i].date + '</strong></div>';
			strYearDivs+= '<div class="numEvents">' + (this.timelineData[i].events.length) + ' ' + singularPlural + ' found</div>';
			strYearDivs+= '</div>';
			this.minYear = this.timelineData[i].year < this.minYear ? this.timelineData[i].year : this.minYear;
			this.maxYear = this.timelineData[i].year > this.maxYear ? this.timelineData[i].year : this.maxYear;
		}
		this.currentYear = this.minYear;
		this.timelineWindowStartYear = this.currentYear;
		//$('#sliderVal').text(this.currentYear);
		this.maxScrollYear = this.maxYear - (objTimeline.itemsToDisplay - 1);
		$('#timeline').html(strYearDivs);
	},
	createTimeline: function()
	{
		$('#slider').slider(
		{
			min: objTimeline.minYear,
			max: objTimeline.maxYear,
			step : 1,
			start : function(event, ui)
			{
				if(objTimeline.isWindowOpen)
				{
					objTimeline.closeWindow();
				}
			},
			slide: function(event, ui)
			{
				objTimeline.currentYear = ui.value;
				//$('#sliderVal').text(objTimeline.currentYear);
			},
			stop : function(event, ui)
			{
					//animate timeline, window and overlays
				if(objTimeline.currentYear >= objTimeline.maxScrollYear)
				{
					objTimeline.timelineWindowStartYear = objTimeline.maxScrollYear;
					$('#timeline').animate(
					{
						left : (objTimeline.timelineData.length - objTimeline.itemsToDisplay) * 200 * -1
					}, 400);

					var yearsToScroll = objTimeline.currentYear - objTimeline.maxScrollYear;
					
					$('#window').animate(
					{
						left : yearsToScroll * 200
					}, 400);
					$('#leftOverlay').show().animate(
					{
						width: (yearsToScroll * 200)
					}, 400);
					$('#rightOverlay').show().animate(
					{
						width: ((objTimeline.itemsToDisplay -1) * 200 ) - (yearsToScroll * 200 )
					}, 400);
				}
				else 
				{
					objTimeline.timelineWindowStartYear = objTimeline.currentYear;
					var yearDiff = Math.abs(objTimeline.currentYear - objTimeline.minYear);
					var newLeft = ((yearDiff * 200)) * -1;
					$('#timeline').animate(
					{
						left : newLeft
					}, 400);					
					
					$('#window').animate(
					{
						left : 0
					}, 400);
					
					$('#leftOverlay').hide();
					
					$('#rightOverlay').show().animate(
					{
						width: (objTimeline.itemsToDisplay * 200 ) - 200
					}, 400);
				}
			}
		});
		
		
		$('#window').draggable(
		{
			containment: '.timeline-container', 
			grid : [200,0],
			cursor: 'pointer',
			drag : function(event, ui)
			{
				var leftPos = ui.position.left;
				$('#leftOverlay').css({width: leftPos}).show();
				$('#rightOverlay').css({width : (objTimeline.itemsToDisplay * 200) - leftPos - 200}).show();
			},
			stop : function(event, ui)
			{
				var leftPos = ui.position.left;
				leftPos = leftPos/200;
				objTimeline.currentYear = objTimeline.timelineWindowStartYear + leftPos;
				$('#slider').slider('value', objTimeline.currentYear);
				//$('#sliderVal').text(objTimeline.currentYear);
			}
		});
		$('#window').click(function()
		{
			if(objTimeline.isWindowOpen)
			{
				return;
			}
			objTimeline.isWindowOpen = true;
			$('.link').hide();
			objTimeline.windowLeft = $(this).css('left');
			$(this).css({'background-image' : 'url(/img/background-puppies.jpg)'})
			.animate(
			{
				left : 0,
				width : (objTimeline.itemsToDisplay * 200 ) + 'px',
				height: '398px'
			}, 100, function()
			{
				$('.timeline-container').css({'border' : 0});
				$('.close').show();
				var str = '<ol>';
				for(var i=0; i <objTimeline.timelineData.length; i++)
				{
					if(objTimeline.timelineData[i].year == objTimeline.currentYear)
					{
						var allEvents = (objTimeline.timelineData[i]).events;
						if(allEvents.length == 0 )
						{
							str+= '<li>No events found.</li>';
						}
						for(var j=0; j< allEvents.length; j++)
						{
							str+= '<li>';
							str+= allEvents[j];
							str+= '</li>';
						}
						break;
					}
				}
				str+= '</ol>';
				$('#yearEvents').html(str);
			});
		});
		
		$('.close').click(function(event)
		{
			//event.stopPropagation();
			objTimeline.closeWindow();
		});
	},
	closeWindow : function()
	{
		$('.timeline-container').css({'border' : '1px solid #333', 'border-left' : 0});	
		$('#yearEvents').empty();
		$('.close').hide();
		$('.link').show();
		$('#window').animate(
		{
			width: '201px',
			height: '398px',
			left : objTimeline.windowLeft
		}, 500, function()
		{
			$(this).css({
				"background-image" : "none",
				"background-color" : 'transparent'
			});
			objTimeline.isWindowOpen = false;
		});
	}
};