var ready;
ready = function() {
  var $window = $(window);
  $window.on('scroll', function() {
    var currentScrollTop = $window.scrollTop();
    var offsetTab = $('#tab').offset().top;
    if(currentScrollTop == offsetTab) {
      $("#evolution-modal").modal('show');   
    }
  });
  
  $('.scroll-down a').on('click', function(e) {
		e.preventDefault();
	    var scrollTo = this.id;
	    var str = scrollTo.slice(3,6);
	    $('html, body').animate({
	      scrollTop: $("#" + str).offset().top
		}, 2000); 
  });
  
  $('.tab-list').each(function(){
  	// hold the current set of tabs
  	var $this = $(this);
  	// hold the currently active tab
  	var $tab = $this.find('li.active');
  	// hold the <a> element within that tab
  	var $link = $tab.find('a');
  	// hold the value of the href attribute for the active tab
  	var $panel = $($link.attr('href'));

    // An event listener is set up to check for when the user clicks on any tab within the list 
    $this.on('click', '.tab-control', function(e){
      // Prevent link behavior 
      e.preventDefault();
      // Store the current link
  	  var $link = $(this);
  	  // Get href of clicked tab
  	  var id = this.hash;

      // If not currently active
  	  if (id && !$link.is('.active')) {
  	  	// Make panel inactive
  	  	$panel.removeClass('active');
  	  	// Make tab inactive
  	  	$tab.removeClass('active');

        // Make new panel active
        $panel = $(id).addClass('active');
        // Make new tab active
        $tab = $link.parent().addClass('active');
  	  }
    });
  });
  
  $('#myCarousel-1').on('slide.bs.carousel', function (ev) {
    var id = ev.relatedTarget.id;
    switch (id) {
      case "tab-1-1":
        $('#tab-1-1-content').addClass('activeContent');
        $('#tab-1-2-content').removeClass('activeContent');
        $('#tab-1-3-content').removeClass('activeContent');
        break;
      case "tab-1-2":
        $('#tab-1-2-content').addClass('activeContent');
        $('#tab-1-1-content').removeClass('activeContent');
        $('#tab-1-3-content').removeClass('activeContent');
        break;
      case "tab-1-3":
        $('#tab-1-3-content').addClass('activeContent');
        $('#tab-1-1-content').removeClass('activeContent');
        $('#tab-1-2-content').removeClass('activeContent');
        break;
    }
  });
  
  $('#myCarousel-2').on('slide.bs.carousel', function (ev) {
    var id = ev.relatedTarget.id;
    switch (id) {
      case "tab-2-1":
        $('#tab-2-1-content').addClass('activeContent');
        $('#tab-2-2-content').removeClass('activeContent');
        $('#tab-2-3-content').removeClass('activeContent');
        break;
      case "tab-2-2":
        $('#tab-2-2-content').addClass('activeContent');
        $('#tab-2-1-content').removeClass('activeContent');
        $('#tab-2-3-content').removeClass('activeContent');
        break;
      case "tab-2-3":
        $('#tab-2-3-content').addClass('activeContent');
        $('#tab-2-1-content').removeClass('activeContent');
        $('#tab-2-2-content').removeClass('activeContent');
        break;
    }
  });

  $('#myCarousel-3').on('slide.bs.carousel', function (ev) {
    var id = ev.relatedTarget.id;
    switch (id) {
      case "tab-3-1":
        $('#tab-3-1-content').addClass('activeContent');
        $('#tab-3-2-content').removeClass('activeContent');
        $('#tab-3-3-content').removeClass('activeContent');
        break;
      case "tab-3-2":
        $('#tab-3-2-content').addClass('activeContent');
        $('#tab-3-1-content').removeClass('activeContent');
        $('#tab-3-3-content').removeClass('activeContent');
        break;
      case "tab-3-3":
        $('#tab-3-3-content').addClass('activeContent');
        $('#tab-3-1-content').removeClass('activeContent');
        $('#tab-3-2-content').removeClass('activeContent');
        break;
    }
  });

  $('#myCarousel-4').on('slide.bs.carousel', function (ev) {
    var id = ev.relatedTarget.id;
    switch (id) {
      case "tab-4-1":
        $('#tab-4-1-content').addClass('activeContent');
        $('#tab-4-2-content').removeClass('activeContent');
        $('#tab-4-3-content').removeClass('activeContent');
        break;
      case "tab-4-2":
        $('#tab-4-2-content').addClass('activeContent');
        $('#tab-4-1-content').removeClass('activeContent');
        $('#tab-4-3-content').removeClass('activeContent');
        break;
      case "tab-4-3":
        $('#tab-4-3-content').addClass('activeContent');
        $('#tab-4-1-content').removeClass('activeContent');
        $('#tab-4-2-content').removeClass('activeContent');
        break;
    }
  });
  
  $('#myCarousel-5').on('slide.bs.carousel', function (ev) {
    var id = ev.relatedTarget.id;
    switch (id) {
      case "tab-5-1":
        $('#tab-5-1-content').addClass('activeContent');
        $('#tab-5-2-content').removeClass('activeContent');
        $('#tab-5-3-content').removeClass('activeContent');
        break;
      case "tab-5-2":
        $('#tab-5-2-content').addClass('activeContent');
        $('#tab-5-1-content').removeClass('activeContent');
        $('#tab-5-3-content').removeClass('activeContent');
        break;
      case "tab-5-3":
        $('#tab-5-3-content').addClass('activeContent');
        $('#tab-5-1-content').removeClass('activeContent');
        $('#tab-5-2-content').removeClass('activeContent');
        break;  
    }
  });
};

$(document).ready(ready);
$(document).on('page:load', ready);

(function ($) {

    $(function (){
        $("#background-cycler").cycle();
    });

    $.fn.cycle = function (options)
    {
        return this.each(function ()
        {
            $.cycle(this);
        });
    };

    $.cycle = function (container) {
        var elements = $(container).children(".img-cycled");

        if (elements.length > 1)
        {
            for (var i = 0; i < elements.length; i++)
            {
                $(elements[i]).css('z-index', String(elements.length - i)).hide();
            }

            setTimeout(function ()
            {
                $.cycle.next(elements, 0);
            }, 2400);

            $(elements[0]).fadeIn();
        }
    };

    $.cycle.next = function (elements, current)
    {
        var next = (current + 1) < elements.length ? current + 1 : 0;
        
        $(elements[next]).fadeIn(1200);
        $(elements[current]).fadeOut(1200);

        setTimeout(function ()
        {
            $.cycle.next(elements, next);
        }, 2400);
    };

})(jQuery);