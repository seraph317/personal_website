$(document).ready(function() {
    var shareBox = {
        init : function()
		{
			this.initSharing();
		},
        initSharing : function()
		{
			$('.shareBox.btn-group a').on('click', function()
			{
				var type = $(this).prop('type');
				shareBox.sharePage(type);
			});
		},
		sharePage : function(shareType)
		{
			var pageUrl = encodeURIComponent(document.location);
			var shareUrl;
			switch(shareType)
			{
				case 'facebook':
					shareUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + pageUrl;
				break;

				case 'twitter':
					shareUrl = 'https://twitter.com/intent/tweet?text=Check out my page&url='+ pageUrl +'&via=v08i';
				break;

				case 'linkedin':
					shareUrl = ' http://www.linkedin.com/shareArticle?mini=true&url=' + pageUrl;
				break;

				case 'google-plus':
					shareUrl = 'https://plus.google.com/share?url=' + pageUrl;
				break;
				
				default :
				return false;
			}
			window.open(shareUrl , '', 'width=600,height=500');
		}
    };
    shareBox.init();
});