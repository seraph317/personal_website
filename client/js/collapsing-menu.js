(function($) {
	$(function() {
		var	$body = $('body'),
			$wrapper = $('#wrapper'),
            $menu = $('#menu'),
		    $menuClose = $('<a class="close">').appendTo($menu),
			$menuToggle = $('.menuToggle');

		$menu.appendTo($body);
		$menuClose.on('click touchend', function(event) {
			event.preventDefault();
			event.stopPropagation();
			$body.removeClass('is-menu-visible');
		});

		$menuToggle.on('click touchend', function(event) {
			event.preventDefault();
			event.stopPropagation();
			$body.toggleClass('is-menu-visible');
		});

		$wrapper.on('click touchend', function(event) {
			if ($body.hasClass('is-menu-visible')) {
				event.preventDefault();
				event.stopPropagation();
				$body.removeClass('is-menu-visible');
			}
		});
	});
})(jQuery);

$(window).bind('scroll', function () {
    var getSection = document.getElementById("insect-1");
    var sectionTop = getSection.offsetTop;
    if ($(window).scrollTop() > sectionTop) {
        $('#header').removeClass('alt');        
    }else {
     $('#header').addClass('alt');
    }
});

$(document).ready(function() {
	$("#insect-modal").modal('show');
	
	$('.spotlight:nth-child(1)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.375)'
        }, 1, function() {
        	$('.butterfly-nutrient').animate({
        		'color': '#ff2500'
        	}, 1);
        	$('.butterfly-title').animate({
        		'color': '#ff2500'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.075)'
        }, 1, function() {
        	$('.butterfly-nutrient').animate({
        		'color': '#ffffff'
        	}, 1);
        	$('.butterfly-title').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.butterfly-nutrient').tooltip(
	{
		items : '.butterfly-nutrient',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strNutrient = '<b><span class="glyphicon glyphicon-pencil"></span>Butterfly food</b><br>Butterflies can eat <b>anything that can dissolve in water</b>. They mostly feed on <b>nectar from flowers</b> but also eat <b>tree sap, dung, pollen, or rotting fruit</b>. <b>Sodium as well as many other minerals</b> is vital for the butterflies reproduction.';
			    return strNutrient;
		}
	});
    
    $('.spotlight:nth-child(2)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.45)'
        }, 1, function() {
        	$('.butterfly-pollinate').animate({
        		'color': '#ff2500'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.15)'
        }, 1, function() {
        	$('.butterfly-pollinate').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.butterfly-pollinate').tooltip(
	{
		items : '.butterfly-pollinate',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strNutrient = '<b><span class="glyphicon glyphicon-pencil"></span>Pollination</b><br>Pollination is the process by which pollen is transferred from the anther to the stigma of the plant, thereby enabling <b>fertilization</b> and <b>reproduction</b>.<br><b>Cross-pollination</b> occurs only when pollen is delivered to a flower from a different plant; <b>self-pollination</b> occurs when pollen from one flower pollinates the same flower or other flowers of the same individual.';
			    return strNutrient;
		}
	});
    
    $('.spotlight:nth-child(3)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.525)'
        }, 1, function() {
        	$('.bee-honey-wax').animate({
        		'color': '#d8cf0f'
        	}, 1);
        	$('.bee-title').animate({
        		'color': '#d8cf0f'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.225)'
        }, 1, function() {
        	$('.bee-honey-wax').animate({
        		'color': '#ffffff'
        	}, 1);
        	$('.bee-title').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.bee-honey-wax').tooltip(
	{
		items : '.bee-honey-wax',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strHoneyWax = '<b><span class="glyphicon glyphicon-pencil"></span>Honey</b><br>A sweet food made by bees using <b>nectar from flowers</b>. Honey bees transform saccharides into honey by a process of <b>regurgitation</b>, a number of times, until it is partially digested.<br><b><span class="glyphicon glyphicon-pencil"></span>Beeswax</b><br>The wax is formed by worker bees, which secrete it from <b>eight wax-producing mirror glands</b> on the inner sides of the sternites on abdominal segments 4 to 7.';
			    return strHoneyWax;
		}
	});
    
    $('.spotlight:nth-child(4)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.6)'
        }, 1, function() {
        	$('.bee-species').animate({
        		'color': '#d8cf0f'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.3)'
        }, 1, function() {
        	$('.bee-species').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.bee-species').tooltip(
	{
		items : '.bee-species',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strSpecies = '<b><span class="glyphicon glyphicon-pencil"></span>Bumble Bee</b><br>Unlike honeybees, bumblebees can <b>sting more than once</b> because their stingers are smooth and do not get caught in the skin when they fly away.<br><b><span class="glyphicon glyphicon-pencil"></span>Carpenter Bee</b><br><b>Solitary bees</b> build nests just for themselves and only feed their own young. They get their name from their ability to drill through wood.<br><b><span class="glyphicon glyphicon-pencil"></span>Killer Bee</b><br>They have been known to chase people for over a quarter of a mile once they get excited and aggressive.';
			    return strSpecies;
		}
	});
    
    $('.spotlight:nth-child(5)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.075)'
        }, 1, function() {
        	$('.mantis-predator').animate({
        		'color': '#ff748c'
        	}, 1);
        	$('.mantis-title').animate({
        		'color': '#ff748c'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.375)'
        }, 1, function() {
        	$('.mantis-predator').animate({
        		'color': '#ffffff'
        	}, 1);
        	$('.mantis-title').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.mantis-predator').tooltip(
	{
		items : '.mantis-predator',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strPredator = '<b><span class="glyphicon glyphicon-pencil"></span>Predator</b><br><b>Insects</b> are their primary prey, but the diet of a mantis changes as it grows larger. Mantises have been observed catching and eating <b>hummingbirds</b>, <b>mice</b> and <b>snakes</b> among other <b>vertebrates</b>.';
			    return strPredator;
		}
	});
    
    $('.spotlight:nth-child(6)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.15)'
        }, 1, function() {
        	$('.mantis-relatives').animate({
        		'color': '#ff748c'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.45)'
        }, 1, function() {
        	$('.mantis-relatives').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.mantis-relatives').tooltip(
	{
		items : '.mantis-relatives',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strPredator = '<b><span class="glyphicon glyphicon-pencil"></span>Termite</b><br>Although these insects are often called white ants, they are <b>not ants</b>. Recent phylogenetic studies indicate that they evolved <b>from close ancestors of cockroaches</b>.<br><b><span class="glyphicon glyphicon-pencil"></span>Mantisfly</b><br>They look like <b>wasp</b>s but have the front legs, sharp jaws and bulging eyes of <b>praying mantis</b>es. This similarity is an example of <b>convergent evolution</b>; mantidflies do not have the leathery forewings of mantises.<br>';
			    return strPredator;
		}
	});
    
    $('.spotlight:nth-child(7)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.225)'
        }, 1, function() {
        	$('.dragonfly-nymph').animate({
        		'color': '#00b300'
        	}, 1);
        	$('.dragonfly-title').animate({
        		'color': '#00b300'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.525)'
        }, 1, function() {
        	$('.dragonfly-nymph').animate({
        		'color': '#ffffff'
        	}, 1);
        	$('.dragonfly-title').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.dragonfly-nymph').tooltip(
	{
		items : '.dragonfly-nymph',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strNymph = "<b><span class='glyphicon glyphicon-pencil'></span>Nymph</b><br>Most of a dragonfly's life is spent as a nymph, beneath the water's surface. The nymph extends its <b>labium (a toothed mouthpart)</b> to catch animals such as <b>mosquito larvae</b>, <b>tadpoles</b> and <b>small fish</b>.";
			    return strNymph;
		}
	});
    
    $('.spotlight:nth-child(8)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.15)'
        }, 1, function() {
        	$('.dragonfly-fossil').animate({
        		'color': '#00b300'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.45)'
        }, 1, function() {
        	$('.dragonfly-fossil').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.dragonfly-fossil').tooltip(
	{
		items : '.dragonfly-fossil',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strNymph = "<b><span class='glyphicon glyphicon-pencil'></span>Giant Dragonfly</b><br>Probably the largest insect that ever lived, <b>M. permiana</b>'s immense size has led researchers to think that it may have fed on animals as large as frogs and squirrels in order to sustain itself. The prehistoric dragonflies are thought to have gone extinct when the earth’s atmosphere started to lose its high oxygen levels millions of years ago.";
			    return strNymph;
		}
	});
    
    $('.spotlight:nth-child(9)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.075)'
        }, 1, function() {
        	$('.rhinoceros-beetle-group').animate({
        		'color': '#d27f38'
        	}, 1);
        	$('.rhinoceros-beetle-title').animate({
        		'color': '#d27f38'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.375)'
        }, 1, function() {
        	$('.rhinoceros-beetle-group').animate({
        		'color': '#ffffff'
        	}, 1);
        	$('.rhinoceros-beetle-title').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.rhinoceros-beetle-group').tooltip(
	{
		items : '.rhinoceros-beetle-group',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strGroup = "<b><span class='glyphicon glyphicon-pencil'></span>Hercules Beetle</b><br>The most famous and the <b>largest</b> of the rhinoceros beetles, up to 17 cm (6.5 in) in length. Hercules beetles are <b>highly sexually dimorphic</b>, with the females generally being larger-bodied but much shorter, as they lack horns entirely.<br><b><span class='glyphicon glyphicon-pencil'></span>Unicorn Beetle</b><br>The eastern Hercules beetle. It's a species of rhinoceros beetle that lives in the Eastern United States.";
			    return strGroup;
		}
	});
    
    $('.spotlight:nth-child(10)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.6)'
        }, 1, function() {
        	$('.rhinoceros-beetle-fight').animate({
        		'color': '#d27f38'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.3)'
        }, 1, function() {
        	$('.rhinoceros-beetle-fight').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.rhinoceros-beetle-fight').tooltip(
	{
		items : '.rhinoceros-beetle-fight',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strGroup = "<b><span class='glyphicon glyphicon-pencil'></span>Stag Beetle</b><br>The English name is derived from the large and distinctive mandibles found on the males of most species, which resemble the antlers of stags.<br> In Japan, the bugs are popular subjects for summer homework and individual research projects. The two biggest favorites are the <b>Japanese rhinoceros beetle</b> and the <b>stag beetle</b>.";
			    return strGroup;
		}
	});
    
    $('.spotlight:nth-child(11)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.525)'
        }, 1, function() {
        	$('.grasshopper-locust').animate({
        		'color': '#bbaf2a'
        	}, 1);
        	$('.grasshopper-title').animate({
        		'color': '#bbaf2a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.225)'
        }, 1, function() {
        	$('.grasshopper-locust').animate({
        		'color': '#ffffff'
        	}, 1);
        	$('.grasshopper-title').animate({
        		'color': '#ffffff'
        	}, 1);
        });
    });
    
    $('.grasshopper-locust').tooltip(
	{
		items : '.grasshopper-locust',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strLocust = "<b><span class='glyphicon glyphicon-pencil'></span>Polymorphism</b><br>Swarming behaviour is a response to <b>overcrowding</b>. Increased tactile stimulation of the hind legs causes an increase in levels of <b>serotonin</b>. This causes the locust to <b>change colour, eat much more, and breed much more easily</b>. The transformation of the locust to the swarming form is induced by several contacts per minute over a four-hour period.";
			    return strLocust;
		}
	});
    
    $('.spotlight:nth-child(12)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.45)'
        }, 1, function() {
        	$('.grasshopper-disaster').animate({
        		'color': '#bbaf2a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.15)'
        }, 1, function() {
        	$('.grasshopper-disaster').animate({
        		'color': '#bbaf2a'
        	}, 1);
        });
    });
    
    $('.grasshopper-disaster').tooltip(
	{
		items : '.grasshopper-disaster',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strDisaster = "<b><span class='glyphicon glyphicon-pencil'></span>Ancient</b><br>The insects arrived unexpectedly, often after a change of wind direction or weather, and the consequences were devastating. The <b>Ancient Egyptian</b>s carved locusts on tombs, and a devastating plague is mentioned in the Book of <b>Exodus</b> in the <b>Bible</b>.";
			    return strDisaster;
		}
	});
	
	$('.features li:nth-child(1)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.175)'
        }, 1, function() {
        	$('.evolution').animate({
        		'color': '#9a9a9a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.35)'
        }, 1, function() {
        	$('.evolution').animate({
        		'color': '#d3d4e4'
        	}, 1);
        });
    });
    
    $('.evolution').tooltip(
	{
		items : '.evolution',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strEvolution = "<b><span class='glyphicon glyphicon-pencil'></span>Ancestor</b><br>The oldest definitive insect fossil is the <b>Rhyniognatha hirsti</b>, from the 396-million-year-old Rhynie chert. It may have superficially resembled a modern-day silverfish insect. This species already possessed <b>dicondylic mandibles</b>, a feature associated with <b>winged insects</b>, suggesting that wings may already have evolved at this time.";
			    return strEvolution;
		}
	});
    
    $('.features li:nth-child(2)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.21)'
        }, 1, function() {
        	$('.arthropod').animate({
        		'color': '#9a9a9a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.07)'
        }, 1, function() {
        	$('.arthropod').animate({
        		'color': '#d3d4e4'
        	}, 1);
        });
    });
    
    $('.arthropod').tooltip(
	{
		items : '.arthropod',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strArthropod = "<b><span class='glyphicon glyphicon-pencil'></span>Largest Phylum</b><br><b>Arthropod</b>s form the phylum Arthropoda, and include the <b>insects, arachnids, myriapods, and crustaceans</b>. Arthropods are invertebrates with <b>segmented bodies</b> and <b>jointed limbs</b>. Estimates of the number of arthropod species vary between 1,170,000 and 5 to 10 million.";
			    return strArthropod;
		}
	});
    
    $('.features li:nth-child(3)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.245)'
        }, 1, function() {
        	$('.diversity').animate({
        		'color': '#9a9a9a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.105)'
        }, 1, function() {
        	$('.diversity').animate({
        		'color': '#d3d4e4'
        	}, 1);
        });
    });
    
    $('.diversity').tooltip(
	{
		items : '.diversity',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strDiversity = "<b><span class='glyphicon glyphicon-pencil'></span>Four Orders</b><br>Of the 24 orders of insects, four dominate in terms of numbers of described species, with at least 3 million species included in <b>Coleoptera (beetles), Diptera (flies), Hymenoptera (sawflies, wasps, bees and ants) and Lepidoptera (moths and butterflies)</b>.";
			    return strDiversity;
		}
	});
    
    $('.features li:nth-child(4)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.28)'
        }, 1, function() {
        	$('.social').animate({
        		'color': '#9a9a9a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.14)'
        }, 1, function() {
        	$('.social').animate({
        		'color': '#d3d4e4'
        	}, 1);
        });
    });
    
    $('.social').tooltip(
	{
		items : '.social',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strSocial = "<b><span class='glyphicon glyphicon-pencil'></span>Superorganism</b><br>A superorganism is an organism consisting of many organisms. The term is used most often to describe a social unit of eusocial animals, where division of labour is <b>highly specialised</b> and where <b>individuals are not able to survive by themselves</b> for extended periods.";
			    return strSocial;
		}
	});
    
    $('.features li:nth-child(5)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.035)'
        }, 1, function() {
        	$('.sense').animate({
        		'color': '#9a9a9a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.175)'
        }, 1, function() {
        	$('.sense').animate({
        		'color': '#d3d4e4'
        	}, 1);
        });
    });
    
    $('.sense').tooltip(
	{
		items : '.sense',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strSense = "<b><span class='glyphicon glyphicon-pencil'></span>Chemical Communication</b><br>Chemical communications in animals rely on a variety of aspects including <b>taste and smell</b>. <b>Chemoreception</b> is the physiological response of a sense organ to a chemical stimulus where the chemicals act as signals to regulate the state or activity of a cell. A <b>semiochemical</b> is a message-carrying chemical that is meant to attract, repel, and convey information.";
			    return strSense;
		}
	});
    
    $('.features li:nth-child(6)').mouseenter( function(event) {
		var targetEnter = $(this);
		$(targetEnter).animate({
		    'background-color': 'rgba(0, 0, 0, 0.07)'
        }, 1, function() {
        	$('.defense').animate({
        		'color': '#9a9a9a'
        	}, 1);
        });
	}).mouseleave(function(event) {
        var targetLeave = $(this);
        $( targetLeave ).animate({
		    'background-color': 'rgba(0, 0, 0, 0.21)'
        }, 1, function() {
        	$('.defense').animate({
        		'color': '#d3d4e4'
        	}, 1);
        });
    });
    
    $('.defense').tooltip(
	{
		items : '.defense',
		position: { my: 'right top', at: 'bottom', collision : 'flipfit' },
		content : function()
		{
			var strDefense = "<b><span class='glyphicon glyphicon-pencil'></span>Camouflage</b><br>Camouflage involves the use of coloration or shape to <b>blend into the surrounding environment</b>.<br><b><span class='glyphicon glyphicon-pencil'></span>Mimicry</b><br>Mimicry is using <b>color</b> or <b>shape</b> to deceive potential enemies.<br><b><span class='glyphicon glyphicon-pencil'></span>Chemical Defense</b><br>Chemical defense is usually advertised by bright colors. They obtain their <b>toxicity</b> by sequestering the chemicals from the plants they eat into their own tissues.";
			    return strDefense;
		}
	});
    
	$('#banner a').on('click', function(e) {
		e.preventDefault();
	    scrollTo = this.id;
	    $('html, body').animate({
            scrollTop: $("#" + scrollTo + "-1").offset().top
	    }, 2000); 
	});
});