(function ($) {

    $(function (){
        $("#background-cycler").cycle();
    });

    $.fn.cycle = function (options)
    {
        return this.each(function ()
        {
            $.cycle(this);
        });
    };

    $.cycle = function (container) {
        var elements = $(container).children(".img-cycled");

        if (elements.length > 1)
        {
            for (var i = 0; i < elements.length; i++)
            {
                $(elements[i]).css('z-index', String(elements.length - i)).hide();
            }

            setTimeout(function ()
            {
                $.cycle.next(elements, 0);
            }, 3000);

            $(elements[0]).fadeIn();
        }
    };

    $.cycle.next = function (elements, current)
    {
        var next = (current + 1) < elements.length ? current + 1 : 0;
        
        $(elements[next]).fadeIn(1500);
        $(elements[current]).fadeOut(1500);

        setTimeout(function ()
        {
            $.cycle.next(elements, next);
        }, 3000);
    };

})(jQuery);