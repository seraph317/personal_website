$(document).ready(function()
{
	myMap.initialize();
    $("#landscape-modal").modal('show');
});

var myMap = {
    map : null,
	markers : [],
	infowindow  : null,
	minVisitor : 0,
	maxVisitor : 0,
	landscape : [
	  {
	    name : 'Taroko National Park',
	    lat : '24.226405',
	    lng : '121.436358',
	    visitor : 4611408,
	    area : 92000,
	    fauna: 'fauna 1251',
	    flora: 'flora 1994',
	    info : 'Taroko Gorge<img src="img/img-taroko.jpg" width="430" class="img-landscape">Taroko National Park is mainly located in the area surrounded by the Liwu River Valley, the eastern and western Cross-island Highways and the outer mountain areas. As the lifeline of Taroko National Park, the Liwu River starts from Hehuan Mountain and the north peak of Cilai Mountain and runs eastward across the mountains and valleys there for a distance of 58.4km before entering the Pacific Ocean in the north of Sinzheng. Over the past millions of years, the Liwu River has been cutting the oldest marble stratum in the geological history of Taiwan here in Taroko and the crust in the area continued to rise. Together with the combined effect of weathering and corrosion, traces of eras and tectonic plate movements over time are seen everywhere in the Taroko Gorge.<br>'
	           + 'Taroko was described as “The most beautiful scene of heaven and earth and the greatest fun of all mountains and rivers.” Entering Taroko National Park is like walking into a splendid and spiritual ink wash painting, with great green mountains, murmuring rivers, thick forests, floating mists and clouds, valleys and mountains, and waterfalls and strange stones.<br>'
	           + 'All have captivated the soul of countless travelers marveling at the greatness of nature.',
	    facility: '<strong>Taroko National Park Headquarters</strong><br>Fushih 291, Fushih Village, Siulin 　Township, Hualien County, Taiwan 97253<br>Service hours: 8:30-17:00<br>Tel: 886-3-8621-100-6 ext. 360 and 361s<br>Fax: 886-3-8621-263<br><a href="http://www.taroko.gov.tw/">Website link</a><img src="img/map-taroko.jpg" width="430" class="img-landscape">',
	    attraction: '<ol><li>Suao-Hualien Coast<p>The section of Suao-Hualien Highway from Heren to Taroko is about 19km and located in the east of Taroko National Park. Along the highway, visitors can appreciate the soaring Cingsui Rift Valley, the splendid Pacific Ocean, and the wonderful scenes where mountains and sea crisscross one another. There are trails reaching the coast for sightseeing at the Heren Border Tablet (168.6KM) and Zhongde Tunnel North Entrance (176.4KM). It takes about 20 minutes’ walk.</p></li>'
	                + '<li>Shakadon Trail<p>Originally called the Mystery Valley Trail, it is now an indigenous culture trail. The entrance is located down the stairs beneath the Shakadon Bridge. It is a path built within a marble cliff. It is about 4.5km to Sanjianwu. Water of the Shakadon River is clear all year round. The beauty of the water and stones are wonderful, and everywhere there is quiet and tranquility.</p></li>'
	                + '<li>Yenzecou and Ciuchudong<p>The entire area is an uplifted coral reef platform filled with reefs and karsts. It is a superb spot for watching the sunset and the vistas of the park.</p></li>'
	                + '<li>Lusui and Tiansiang<p>Lusui and Tiansiang seem to be perpetually in spring and have display centers, trails and accommodations. They are the important recreational locations in the gorge area. Plums and Sakura blossom at the chill in early spring. Yellow and red spots decorate the great green forest in late autumn to make the gorge even more romantic.</p></li>'
	                + '<li>Green magic tree<p>The green magic tree in the foggy forest is a gigantic Lunta fir of about 3,200 years old. It is the only thousand-year-old tree on the Central Cross-island Highway. There are thick forests around with a wide range of species. In spring we can see the Rhododendron latoucheae Franch. & Finet, a breed of Rhododendron formosanum Hemsl., blooms all round. In autumn and winter, deciduous trees like the Acer palmatum Thunb. var. pubescens Li. and Acer rubescens/Acer morrisonense decorating the forests with their reddish leaves to embellish the entire area.</p></li>'
	                + '<li>Hehuan Mountain<p>This is a famous spot for snow watching in winter due to its unique land formation However, flowers there blossom beautifully and luxuriantly. Visitors can enjoy ecological activities from the high-mountain trails on the Shihmen Mountain, East Hehuan Mountains and North Hehuan Mountains.</p></li>',
	  },
	  {
	    name : 'Kenting National Park',
	    lat : '21.941928',
	    lng : '120.779074',
	    visitor : 4944137,
	    area : 33289,
	    fauna: 'fauna 4176',
	    flora: 'flora 1931',
	    info : 'Sea coast<img src="img/img-kenting.jpg" width="430" class="img-landscape">Since 5000~6000 years ago, an ethnic group of mariners already cultivated the Austronesian civilization in this place surrounding by mountain and sea.<br>In the mid-18th century, this caudal fin in the south was once a part of the world stage.<br>The tropical climate here has nourished an exuberant flora. The downhill wind ushers in many migratory birds every winter and makes Kenting National Park a sacred place for bird watching.<br>Combining the pulse of the mountains and the breath of the sea, Kenting National Park has too many beautiful scenes to be fully appreciated all at once.<br> Kenting National Park has gorgeous mountain and river views, magnificent marine scenery and distinguished cultural assets. It is an ideal place for visitors who enjoy traveling and recreation, and is a perfect spot for professionals to experience natural landscapes.<br> Blue sky, sunshine, azure sea and white sand are the best illustrations of this tropical national park which affords citizens the opportunity to see a blue coral reef without traveling overseas.',
	    facility: '<strong>Kenting National Park Headquarters</strong><br>596 Kenting Road, Hengchun Township, Pingtung County, Taiwan 94644<br>Service hours: 8:30-17:00<br>Tel: 886-8-886-1321<br>Fax: 886-8-886-2047<br><a href="http://www.ktnp.gov.tw">Website link</a><img src="img/map-kenting.jpg" width="430" class="img-landscape">',
	    attraction: '<ol><li>Baisha Bay<p>This is a beautiful and quiet bay with a clean, white sandy beach about 400m long and a platform barrier in the east. It is quiet even in winter and is perfect for all kinds of beach and marine recreational activity.</p></li>'
	                + '<li>Maobitou<p>As a cape in the west of southern Taiwan, Maobitou is a typical coral reef abrasive landform overlooking another cape, Eluanbi across the Bashi Channel. It is called the “tip of the cat’s nose” because the shape of the adjacent coral rock fallen from the cliff.</p></li>'
	                + '<li>Guanshan<p>The entire area is an uplifted coral reef platform filled with reefs and karsts. It is a superb spot for watching the sunset and the vistas of the park.</p></li>'
	                + '<li>Longluan Lake<p>This deep-water pool has an extensive surface. The water is ordinarily used for irrigation and attracts migratory birds in autumn and winter, and has therefore become a paradise for birds. The nature center on the west bank is the first bird ecology museum in Taiwan and provides exhibitions of bird ecology and dynamic bird observation activities.</p></li>'
	                + '<li>South Bay<p>Previously known as the dabanle, it was a whale-hunting site during the Japanese colonization and is now a fishing village. Its clean and pure beach of about 600m long is ideal for all kinds of beach activities.</p></li>'
	                + '<li>Kenting Forest Recreation Area<p>Previously known as the Kenting Park, it was developed as a tropical botanical garden during the Japanese colonization. There are uplifted coral reefs and thick forests in the area. Besides the native species, there are imported tropical plants. Wonderful scenes, such as the limestone cave, a rift valley, the buttressed root of the looking-glass tree and the pending banyan valley are all worth visiting.</p></li>'
	                + '<li>Sheding Nature Park<p>The entire area is an uplifted coral reef covered with a coral reef rift valley, limestone cave and grassland. The wind shaped trees always facing the northeast monsoon are like natural bonsai looking trees.</p></li>'
	                + '<li>Shadao<p>A shell sand beach about 220m long, the islet contains high levels of calcium carbonate, at 97%. Therefore, it is a protected area of the national park. On the islet there is a shell sand exhibit house for visitors to learn more and see more about shell sand.museum in Taiwan and provides exhibitions of bird ecology and dynamic bird observation activities.</p></li>'
                    + '<li>Eluanbi Park<p>Eluanbi is a cape located in the furthest southern tip of Taiwan at the intersection of the Bashi Channel and Pacific Ocean. The park is covered with coral rocks, unique uplift coral plants and tropical marine plants. Besides the well-known lighthouse, there is a prehistoric site from about 5000 years ago.</p></li>'
                    + '<li>Fongchueisha<p>The dry and wet seasons on Hengchuan Peninsula are clearly divided. The southwestern air current in summer brings in lots of rain to the region, and the rain washes away sand on the peninsula into the ocean. The powerful downhill wind in winter brings the sand back to the top of the cliff from the shore. This makes for a unique weathering landform in the area.</p></li>',
	  },
	  {
	    name : 'Yangmingshan National Park',
	    lat : '25.198164',
	    lng : '121.556467',
	    visitor : 4195025,
	    area : 11338,
	    fauna: 'fauna 891',
	    flora: 'flora 1774',
	    info : 'Calla lily flower fields<img src="img/img-yangmingshan.jpg" width="430" class="img-landscape">Yangmingshan National Park is situated between the subtropical and temperate zones. The additional impact of the northeast monsoon leads to weather patterns that shape the unique scenic spots of Yangmingshan National Park.<br>'
	           + 'In early spring, the green sprouts of azalea Rhododendron and Formosan Sweet Gum bring varied colors and great vitality to Yangmingshan. In summer, after misty rains, Cingtiangang grassland is full of the fragrance of green grass. When autumn comes, the red spiked silver grass sway in the wind along Mt. Datun, Mt. Cising and Cingtiangang make up a picture of the 「Datun Autumn Beauty」. At the end of the year,northeast monsoon brings cold wind and drizzle; the moving clouds and fog to make it a poetic landscape.<br>'
	           + 'Picturesque Yangmingshan National Park is unique in its volcanic topography as well as in its close proximity to densely populated Taipei city.',
	    facility: '<strong>Yangmingshan National Park Headquarters</strong><br>1-20, Jhuzihhu Rd.,Yangmingshan, Taipei City, Taiwan 11292<br>Service hours: 8:30-17:00<br>Tel: 886-2-2861-3601<br>Fax: 886-2-2861-1504<br><img src="img/map-yangmingshan.jpg" width="430" class="img-landscape">',
	    attraction: '<ol><li>Siaoyoukeng<p>Siaoyoukeng is located at the foot of Mt. Cising and above Datun Bridge on Yangjin Highway with an elevation of about 800 meters. It is a post-volcanic geological scenic spot, with views of an active fumarole, crystallized sulfur and the resulting landscape.</p></li>'
	                + '<li>Mt. Cising<p>Mt. Cising is the its highest peak in Yangmingshan National Park and the Taipei City area. The mountain top was once a volcanic crater but in time has eroded into seven smaller summits with the main peak having an elevation of 1120 meters.</p></li>'
	                + '<li>Yangmingshuwu<p>Yangmingshuwu (originally named Chungsin Guest House), is located on Chungsin Rd. close to Yangjin Highway. It covers an area of about 15 hectares and was built during 1969-1970. Today it is one of the important historic and cultural sites in the park.</p></li>'
	                + '<li>Erzihping<p>Three ponds are located in the lowlands between Mt. Datun and Mt. Erzih in the western area of Yamgmingshan National Park and are often covered in a mist.</p></li>'
	                + '<li>Datun Nature Park<p>Datun Nature Park, is located between north Mt. Datun and Mt. Caigongkeng, and is next to county road 101 (Bailaka Road). This volcanic basin features regenerated temperate zone broadleaf forests.</p></li>'
	                + '<li>Lengshuikeng<p>Lengshuikeng is located at the east side of Mt. Cising, and is now a dry lakebed dammed by lava from Mt. Cising and Mt. Cigu. The hot spring temperature here is less than 40℃, lower than the 90℃ in other areas in the Park, thus the name Lengshuikeng (cold water pit). It is the only mine bed of solid sulfur granules in Taiwan. </p></li>'
	                + '<li>Jyuansih Waterfall<p>Jyuansih Waterfall is the upstream source of Neishuang Creek, with an elevation of about 14 meters. The waterfall falls as softly and elegantly as spun silk, and the rocks below take on a rusty color due to ferromagnesian oxidization.</p></li>'
	                + '<li>Cingtiangang<p>Cingtiangang is a grassland terrace formed by northward flowing lava from Mt. Jhugao The location was a grazing area for cows in the early years It is now managed by the Taipei City Agricultural Association. The grassland is mainly covered with Carpet Grass and Crenate-leaved Eurya,which stretches for several miles and is a good place for holidays in northern Taiwan.</p></li>'
	                + '<li>Tianxi Park Ecological Educational Center<p>Tianxi Park is situated in at the southeast area of Yangmingshan National Park. It is part of the subtropical broadleaf forest ecology influenced by the northeast monsoon. The various streams, luxurious forests, and close proximity to the Neishuang Creek in Taipei City provide many educational and tourism opportunities.</p></li></ol>',
	  },
	  {
	    name : 'Yushan National Park',
	    lat : '23.406160',
	    lng : '121.019456',
	    visitor : 1263581,
	    area : 103121,
	    fauna: 'fauna 1024',
	    flora: 'flora 2522',
	    info : 'The beauty of Yushan<img src="img/img-yushan.jpg" width="430" class="img-landscape">Yushan penetrates vertically along the south of the Central Mountain Range and runs horizontally across the northeast of Mabo Vertical Fault. It has been the heart of Taiwan ever since ancient times, acting as the ridge of Taiwan.<br>'
	           + 'A third of the famous mountains and valleys on the island are located in the park area containing old geological structures. The spectacular topological landscapes, comprehensive animal, and plant ecologies surprise all who come in contact with them. Experiencing the ancient geological development through the discovery period of Western civilization, through to the massive movement of indigenous people to racial integration, Yushan has accumulated a rich and deep cultural heritage, including the Batongguan Ancient Trail that left behind from the Qing Dynasty and the culture and traditions of the Tsou and Bunun tribes.'
	           + 'The value of Yushan National Park, founds on its unearthed mountain and river landscapes and rich cultural and historical links, is incomparable and it is considered as the best amongst all national parks.',
	    facility: '<strong>Yushan National Park Headquarters</strong><br>300 Jhongshan Road Section 1, Suili Township, Nantou County, Taiwan 55304<br>Service hours: 8:30-17:00<br>Tel: 886-49-2773-121<br>Fax: 886-49-2348-274<br><a href="http://www.ysnp.gov.tw/">Website link</a><img src="img/map-yushan.jpg" width="430" class="img-landscape">',
	    attraction: '<ol><li>Yushan</li><p>The Yushan Mountain Range is located in the central of Taiwan Island. It has overlapping peaks, soaring mountains, steep river valleys with vegetations that varies as the altitude changes. There are three climates on the mountain, subtropical, temperate and cold. The faunae changes with the climate; there are also different kinds of wildlife. The main peak is 3952m above sea level, the tallest in Taiwan and NE Asia. The Taiwanese consider the mountain sacred.</p>'
	                + '<li>Tatajia<p>The New Central Cross-island Highway in Tatajia is the gateway to the northwest of Yushan National Park and to mountain climbing. The highest point of the New Central Cross-island Highway is located at about 2600 above sea level at the intersection of Provincial Highway Nos. 18 and 21. Many trails have been planned there, such as the trails toward the Dongpu Prairie, Lulinshan and Cizishan. The spectacular and diversified ecological resources along the trails are suitable for hiking.</p></li>'
	                + '<li>Batongguan<p>Batongguan is the water divide that one must take when visiting the Chenyaolan River and Laonon River. It is also the intersection of the Batongguan Historic Trail built during the Qing Dynasty and the Batongguan Traversing Trail built by the Japanese during the their colonization. There are rich cultural and historical relics, prairie landscapes and animal habitats. It is the favorite of visitors when flowers are blossoming in spring.</p></li>'
	                + '<li>Tianzi<p>The Southern Cross-island Highway is the access to the major recreational spots in the south of Yushan National Park. The Meishan Recreational Area along the highway is filled with Bunun culture. The Tianzi Recreational Area is a high-mountain lake with quiet scenes. The Jhongziguan Trail is an ideal spot for leisure and hiking.</p></li>'
	                + '<li>Walami<p>Walami in Nanan is a famous attraction in the east of Yushan National Park. It is the hometown of the Formosan black bears. The Walami Trail forms the east section of the Batongguan Cross-mountain Route built by the Japanese during the their colonization and is the ideal trail for ecology visitors.</p></li>'
	  },
	  {
	    name : 'Shei-Pa National Park',
	    lat : '24.400954',
	    lng : '121.157459',
	    visitor : 718836,
	    area : 76850,
	    fauna: 'fauna 815',
	    flora: 'flora 1304',
	    info : 'Dabajian Mountain<img src="img/img-shei-pa.jpg" width="430" class="img-landscape">Landforms in the park are mainly mountains and valleys. Syue Mountain, Dabajian Mountain and Wuling Quadruple Mountains are all above 3,000 meters high. Syue Mountain (3,886 meters) is the highest peak of Syue Mountain Range and also the second highest mountain in Taiwan. Shei-Pa National Park is named by combining one characters from this mountain and another from the “World Unique Peak”— Dabajian Mountain. Dajia River valley, Jiayang Alluvial Fan & river terrace, and meander core bring colors to park sceneries as well.<br>'
	           + 'The Park has distinct four seasons due to the distribution of landform and humidity amid river valley. Early spring rain and plum rain make the park very misty, while in late spring, the mountain is full of wild flowers. In summer, typhoon and convective rain dominate. In autumn, temperature would fall drastically at night when cold air mass moves south and frost can be seen everywhere in the early morning to shape a colorful picture with a mountain full of red foliage and the azure sky. Snow is common in winter, delivering a rare snow view in Taiwan.<br>'
	           + "Complex and diversified landforms, hydrology, and climate have conserved abundant biological resources. There are altogether 26 endemic species of animals living in Shei-Pa National Park, including the valuable and rarely seen Broad-tailed Swallowtail Butterfly, Formosan Landlocked Salmon, Guanwu Formosan Salamander, and Formosan Black Bear, as well as endemic birds like White-throated Laughing Thrush, Formosan Hill Partridge, Mikado Pheasant, Swinhoe's Pheasant, and Formosan Yuhina. There are more than 1,100 species of vascular plants, of which 61 are rare plants, such as Taiwan Sassafras, Devol’s Balsamine, Dumasia miaoliensis, and Botrychium lunaria.",
	    facility: '<strong>Shei-Pa National Park Headquarters</strong><br>100, Shueiweiping, Fusing Village, Dahu Township, Miaoli County, Taiwan 36443<br>Service hours: 8:30-17:00<br>Tel: 886-37-996-100<br>Fax: 886-37-996-302<br><a href="http://www.spnp.gov.tw/">Website link</a><img src="img/map-shei-pa.jpg" width="430" class="img-landscape">',
	    attraction: "<ol><li>Wuling Recreation Area<p>Located in an ever-misty valley called “Shangri-La”, the whole region is mountainous with an average annual temperature of 15℃, which is cool and agreeable. Wuling is very steep but beautiful; the valleys of Cijiawan Creek, Wuming Creek, and Wuling Creek form a canyon; in the north. The four famous magnificent mountains on the ridge of Syue Mountain form the Wuling Quadruple Mountains. Along Taoshan Waterfall Trail, there are various red leaf plants, such as Taiwan Red Maple, Formosan Sweet Gum and Green Maple and animals such as Large-billed Crow, Taiwan Firecrest, and White's Ground Thrush can also be observed.</p></li>"
	                + '<li>Guanwu Recreation Area<p>Guanwu Recreation Area is situated at an average altitude of 2,000 meters with an average annual temperature of 15℃. This area faces the valley formed by Ban Mountain, Jhen Mountain, and Sanrong Mountain. When humidity at lower altitude rises to this region, as the temperature drops, water vapor turns into fog to mist the whole valley. That is the reason why it is called Guanwu (seeing fog) in Chinese. There are altogether six major trails in the region: Yulun Trail, Kuaishan Big Trees Trail, Guanwu Waterfall Trail, Jhenshan Trail, Leshan Forest Road and East Branch of Dalu Forest Road. In the view-watching platforms and trails behind Guanwu Visitor Center, visitors can have a grand view of the magnificent towering rock ridge of Shengling Ridge. April to June is the best time for bird watching. Commonly observable species include Formosan Yuhina, Yellow-throated Minivet, Green-backed Tit, and White-eared Sibia.Butterfly season starts in early spring. There are Graphium eurous asakurae; in spring and Papilio hoppo with Highland Red-belly Swallowtail Butterfly in summer.</p></li>'
	                + '<li>Syuejian Recreation Area<p>It is a natural recreation area in the park and at the altitude of between 800 and 2,100 meters. Due to the landform and slope direction, visitors can have a full view of the ridges of the magnificent Shengling Ridge and Dasyue Mountain in the park. Together with the abundant hydrological resources and well-preserved forests, it provides wildlife with a conducive natural environment. The areas along Beikeng Creek Historic Trail were gathering places of the Atayal people. Therefore, Atayal cultural relics and historical remains belonging to the Japanese colonial period are abundant.</p></li></ol>',
	  },
	  {
	    name : 'Sun Moon Lake National Scenic Area',
	    lat : '23.858661',
	    lng : '120.915620',
	    visitor : 4745273,
	    area : 18100,
	    fauna: 'fishes 24',
	    flora: 'flora 930',
	    info : 'The Sun Moon Lake Cherry Blossom Festival<img src="img/img-sun-moon-lake.jpg" width="430" class="img-landscape">Sun Moon Lake, situated in Nantou County’s Yuchi Township near the center of Taiwan, is the island’s largest lake. This beautiful alpine lake is divided by tiny Lalu Island; the eastern part of the lake is round like the sun and the western side is shaped like a crescent moon, hence the name "Sun Moon Lake".<br>'
	           + 'Its crystalline, emerald green waters reflect the hills and mountains which rise on all sides. Natural beauty is enhanced by numerous cultural and historical sites. Well-known both at home and abroad, the Sun Moon Lake Scenic Area has exceptional potential for further growth and recognition as a prime tourism destination.<br>'
	           + 'Its beauty is created by the combination of mountain and water scenery, and its 760-meter elevation helps give the impression of a Chinese landscape painting with mist-laden water and clearly defined levels of mountains. The constant changes of mists and moods on the lake make it impossible to comprehend in a single look, and thus, visitors like to linger here.<br>'
	           + 'The beauty of the lake, from dawn to dusk and from spring and summer to autumn and winter, exudes an aura of enchantment regardless of whether it is bathed in sunlight or shrouded in mist. Consequently, viewers never tire of looking at it admiration and amazement.<br>'
	           + "In the early morning the surface of the lake is covered by a thin veil of mist. When the sun rises over the mountains the obscuring mist lifts slowly to reveal the lake's true countenance, with its surface reflecting images of mountains and trees as chirping insects and singing birds lend their voices to the enchanted scene. At dusk the colors of the setting sun paint the lake a sparkling gold, transforming it into a crystal fairyland. As the curtain of night falls, the lights and reflections ripple with the waves and cooling evening breezes waft in, giving rise to fascinating illusions of spirits sprinkling fairy dust on the water.<br>"
	           + 'If you want to discover the many faces of Sun Moon Lake, take a stroll along its shores and let your soul be moved by its inexhaustible beauties. You can also ride a bicycle along the road circling the lake, and soak up the tranquility and peace of nature. Or you can board a boat and cruise over the waves as you take in the glories of the mountains and waters. If you want, you can rent a rowboat and enjoy the scenery at your leisure.<br>'
	           + "Taiwan’s Sept. 21 earthquake, which struck the area in 1999, caused severe damage to the Sun Moon Lake Scenic Area, at the time under the administration of Taiwan’s provincial government. The park has since been reorganized as a National Scenic Area. Significant increases in human and capital investment inflows and the promotion of various special programs and events centered on the Scenic Area have been instrumental in attracting tourists, helping the area emerge from the shadow of the earthquake and setting Sun Moon Lake firmly back on the path to becoming an international tourist destination.",
	    facility: '<strong>Sun Moon Lake National Scenic Area Administration</strong><br>599, Jhongshan Rd., Yuchih Township, Nantou County 555, Taiwan<br>Tel:886-49-2855668<br>Fax:886-49-2855593<br><a href="http://www.sunmoonlake.gov.tw/">Website link</a><img src="img/map-sun-moon-lake.jpg" width="430" class="img-landscape">',
	    attraction: "<ol><li>Aboriginal Singing and Dancing Performances<p>Many tourists take boats to Dehua Village to enjoy the dancing and singing of the Thao tribe, and also to satisfy their curiosity. Viewing the performances, tourists can learn more about the culture of the Aboriginal peoples. In turn, the Thao tribe can benefit economically from the tourists. Of all the Aboriginal tribes, the Thao tribe at Sun Moon Lake was the first one to participate in tourist activities. Many Han businessmen have also come here to do business, and Dehua Village is now the richest village in the Sun Moon Lake area. After Taiwan's retrocession, instead of being classified as an Aboriginal area, Dehua Village was was classified as a regular village. Thus, both Han people and Thao people live in Dehua Village. The Han culture has greatly influenced the Thao tribe. However, the Thao firmly maintain their ancestral-spirit based religion, and by doing so, have successfully preserved their culture.</p></li>"
	                +'<li>The Formosan Aboriginal Culture Village<p>The Formosan Aboriginal Culture Village first opened on July 27, 1986. It earned itself a reputation for its popular theme area which introduces Taiwanese Aboriginal culture and for its lush forest and beautiful European Garden. Since 1992, an amusement park and other entertainment attractions have been added to the village. The village has both educational and entertainment functions. Since its first opening, tourists have come to the village in droves. The establishment of the Formosan Aboriginal Culture Village helped Sun Moon Lake to regain its tourists. </p></li>'
	                + '<li>The building of the Matchmaker Pavilion on Lalu Island<p>With the development of tourism, the Taiwan Provincial Government declared Sun Moon Lake to be a "Provincial Scenic Area". In order to proceed with the construction of Sun Moon Lake and to satisfy the needs of the tourists, the Nantou County Government established the Nantou County Scenic Area Administration. The construction of the Matchmaker Pavilion on Guanghua (Lalu) Island was part of their plans. Professor Huang Huo-shou, who had come from Mainland China, felt that in all aspects, the scenery of Sun Moon Lake compared favorably with West Lake in Mainland China, except that it lacked a “Matchmaker Pavilion”. So he gave Nantou County magistrate Yang Chao-bi the suggestion of building a Matchmaker Pavilion, and construction was completed in 1978. To promote economical marriages and to publicize the beautiful scenery of Sun Moon Lake, the Nantou County Government holds group marriages on the water every year at the Matchmaker Pavilion, which attracts the participation of many couples.</p></li>',
	  },
	  {
	    name : 'Alishan National Scenic Area',
	    lat : '23.508863',
	    lng : '120.802104',
	    visitor : 2761479,
	    area : 41520,
	    fauna: 'birds 101',
	    flora: 'flora named Alishan over 100',
	    info : 'Three Generation Tree<img src="img/img-alishan.jpg" width="430" class="img-landscape">Alishan is blessed with bountiful natural resources, such as sun rise views, a sea of clouds and old forests. Especially as a popular folk song goes, Alishan has amazed people with its “lush green mountains and deep blue waters.” No matter in spring, summer, autumn or winter, visitors can expect to appreciate some of its beauty. For years, Alishan has been famous for its historical forest railway and indigenous tribes, too.<br>'
	           + 'The Alishan National Scenic Area Administration was established in July 2001. It serves the four townships of Meishan, Zhuqi, Fanlu and Alishan which cover 41,520 hectares of land. Since its foundation, the Administration Office has devoted to integrating tourism resources in the region and improving early-day facilities. New sites have opened and thematic tours are available nowadays. A range of marketing activities have also been held to build a new image of Alishan and boost local businesses.'
	           + "Since taking office on June 3, 2015, the administration has been trying to turn the National Alishan Scenic Area into an international mountain vacation site that’s safe, ecological, sustaining, and of the LOHAS spirit. Based on the existing work, the administration has actively promoted Chiayi as Alishan’s portal city and Provincial Highway 18 as an international tourist road. Taiping Suspension Bridge, Niupuzi Recreational Area and Fenqi Lake and its nearby refurbished town are alishan's three most touted travel highlights. Northwestern Corridor ecological tours and indigenous Tsou trips are much recommended, too. Quality, uniqueness and diversity being our core values, the administration hopes to bring Alishan to a whole new level.",
	    facility: '<strong>Alishan National Scenic Area Administration</strong>51, Checheng, Fanlu Township, Chiayi County 602, Taiwan<br>Fax:886-5-2594305<br>Tel:886-5-2593900<br><img src="img/map-alishan.jpg" width="430" class="img-landscape">',
	    attraction: '<ol><li>Railway Museum<p>Old locomotives rest quietly in the old station shed after a long and illustrious career. The stories of 100 glorious years of Fenqihu’s railway start to be told here.</p></li>'
	                + '<li>Muma Wooden Plank Trail<p>This started out as a timber transport road and is now a trail for visitors.</p></li>'
	                + '<li>North Side Trail Scenery Viewing Platform<p>This is the best high-point from which to view the mountain town Fenqihu and the busy railway station.</p></li>'
	                + '<li>Shinto Temple Remains<p>The temple was built in the Japanese Colonial Period. All that remains today are its base and stone steps. It has an excellent position and wide views can be enjoyed.</p></li>'
	                + '<li>Luding Sacred Tree<p>This a flying moth tree called Japanese Elaeocarpus, also called “ghost tree” by locals, it is over 200 years old.</p></li>'
	                + '<li>Gaozaikeng Historic Trail<p>This trail originated in the Qing Dynasty. A peasant farmer stole a gaozai cake because he was hungry and was detained however the shop owner took pity on him and didn’t send him to the two to be prosecuted, instead making him build the trail, using hard labor to atone for his sin.</p></li>'
	                + '<li>Square Bamboo<p>This variety of bamboo was introduced in 1924, adapted well and has thrived.</p></li>'
	                + '<li>Old Year Hododendrons<p>Three rhododendron bushes over 100 year old.</p></li>'
	                + '<li>Old Street Tour<p>Fenqihu is known as the Jiufen of south Taiwan; the calls of the railway lunch box sellers and the aromas emanating from the cake shops will make your mouth water.</p></li>'
	                + '<li>Taiwan Incense-Cedar<p>After devouring a tasty railway lunchbox take a stroll to the Taiwan Incense cedar viewing platform and have a rest!</p></li>'
	                + '<li>Fenqihu Culture & History Hall<p>This charming Japanese style building was formerly as police building in the Japanese Colonial period. The hall contains a large amount of local cultural and historical materials and images.</p></li>'
	                + '<li>Taiwan Wooden Plank Trail<p>Passing through a Taiwania wood on the south side of Fenqihu, locals call the trail “dream lake path.”<p/></li>',
      },
	  {
	    name : 'Dongshi Forest Garden',
	    lat : '24.285403',
	    lng : '120.867640',
	    visitor : 263337,
	    area: 225,
	    fauna: 'fireflies 9',
	    flora: 'flora 337',
	    info : 'Fireflies watching<img src="img/img-dongshi.jpg" width="430" class="img-landscape">Dongshi Forest Garden or called Tungshih Forest is located in Taichung. The over 200 hectares resort spot is nicknamed “Four Corners Forest” due to the lustrous greenery. Only 8 km from the Tungshih Town center, there are many roads and hiking routes leads to the forest farm. The farm was originally a reforested land, after twenty to thirty years of careful cultivated, the land is now covered with dense forest and farmland. At an altitude of 500 meters, the area is a wonderful getaway from the summer heat.<br>'
	           + 'Dongshi Forest Garden scenery changes distinctively throughout four seasons. The Youth Activity Center in the farm is the center of all activities. It is well equipped with BBQ pits, camping ground, cottages, forest pool, horse riding course, orange farm, Mysterious Valley and Lover’s Valley. It also has the largest out in the wild physical training center in the county. The facilities are laid according to age, gender and physique; all in all, eight sections. Among these facilities are the pool games, obstacle course for the handicappers, artificial skiing lanes and more.'
	           + 'Tungshih Forest is also well known recently for the fireflies watching. The forest is so well managed, clean source of water and environment is the best habitat for fireflies. During March and April, would be the best time to come fireflies watching. Other interesting insects like stag beetles and many more are there to be discovered. With the rich natural resource, Dongshi Forest Garden is a wonderful visit to enjoy for everyone in your family.',
	    facility: 'Opening hours:6:30-22:00<br>6-1, Shilin St., Dongshi Dist., Taichung City 423, Taiwan<br>Telephone:886-4-25872191<img src="img/map-dongshi.jpg" width="430" class="img-landscape">',
	    attraction: 'Dongshi includes some greenspace and significant farmland; largely orchards. Dongshi is known for its pears, which are large and almost spherical with a thin, light yellowish-brown rugose skin. In a good year, their flavor is excellent. Intensive topwork at the start of each season involves grafting Japanese pear bud slips to the tree stock.<br>'
	                + 'The old train station has been converted into the Dongshih Hakka Cultural Park. Historical Hakka cultural artifacts along with modern works of art are on display. This area also marks the southern terminus of the Dongshih-Fongyuan Bicycle Greenway. This greenway was converted from a former railway track. The town is also noted for two large forest parks in the mountainous eastern parts of the county. Dongshi Forest Park and Sijiaolin are managed by the Changhua County and Taichung City Agricultural Committees respectively.',
	  },
	],
	
	initialize : function() {
		this.setMinMaxVisitors();
		this.displayLandscapes();
		this.setSpinner();
		this.createMap();
		this.setMarkersAndInfoWindow();
		this.setSlider();
		this.setIcon();
	},
	
    createMap : function() {
	  var pinLocation = new google.maps.LatLng(23.809675, 121.030109);
      var myOptions = {
        zoom: 8,
        center: pinLocation,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // Allow panning across the map
        panControl: false,
        // Set the zoom level of the map
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.SMALL,
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        // Switch map types
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.TOP_CENTER
        },
        // Show the scale of the map
        scaleControl: true,
        scaleControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER
        },
        // A Pegman icon that can be dragged and dropped onto the map to show a street view
        streetViewControl: false,
        // A thumbnail showing a larger area, that reflects where the current map is within that wider area 
        overviewMapControl: false,
      };
      this.map = new google.maps.Map(document.getElementById("landscape-map-canvas"), myOptions);
      this.infowindow = new google.maps.InfoWindow();
	},

	setMarkersAndInfoWindow : function(){

	  for(var i = 0; i < this.landscape.length; i++)
		{
			var $landscape = this.landscape[i];
			var $marker = new google.maps.Marker({
					position: new google.maps.LatLng($landscape.lat, $landscape.lng),
					map: this.map,
					title: $landscape.name,
					icon: "http://maps.gstatic.com/mapfiles/markers2/marker.png"
			});
			this.markers.push($marker);
			
			google.maps.event.addListener($marker, 'click', function($marker, $landscape) {
				return function() {
					var content = $('#tabs').html();
					myMap.infowindow.setContent('<div id="landscapeFeatures"><h3>' + '<span class="glyphicon glyphicon-tree-deciduous"></span>' + $landscape.name + '</h3>' + content 
					                            + '<div id="info" style="height: 100%;">' + $landscape.info + '</div>'
					                            + '<div id="facilities" style="height: 100%;">' + $landscape.facility + '</div>'
					                            + '<div id="attractions" style="height: 100%;">' + $landscape.attraction + '</div>'
					                            + '</div>');
					myMap.infowindow.setOptions({maxWidth : 500});
					myMap.infowindow.open(myMap.map, $marker);
				};
			}($marker, $landscape));
		}
		
	  google.maps.event.addListener(myMap.infowindow, 'domready', function() {
		$('#landscapeFeatures').tabs();
	  });
	  
	},
	
	setMinMaxVisitors : function()
	{
	    this.minVisitor = this.landscape[0].visitor;
		this.maxVisitor = this.landscape[0].visitor;
		for(var i = 0; i< this.landscape.length; i++)
		{
			this.minVisitor = this.landscape[i].visitor < this.minVisitor ? this.landscape[i].visitor : this.minVisitor;
			this.maxVisitor = this.landscape[i].visitor > this.maxVisitor ? this.landscape[i].visitor : this.maxVisitor;
		}
		$('#landscape-currentRange').text(this.minVisitor + ' vistors - ' + this.maxVisitor + ' visitors');
	},
	
	displayLandscapes : function()
	{
		var str = '';
		for (var i = 0; i < this.landscape.length; i++) {
			var $landscape = this.landscape[i];
			str += '<h3 data-visitor="' + $landscape.visitor + '">' + $landscape.name + '</h3>';
			str += '<div>';
			str += '<div class="ui-state-highlight ui-corner-all" style="padding: 5px;">' + $landscape.visitor + ' visitors</div>';
			str += '<div class="ui-state-highlight ui-corner-all" style="padding: 5px;">' + $landscape.area + ' hectares</div>';
			str += '<div class="ui-state-highlight ui-corner-all" style="padding: 5px;">' + $landscape.fauna + ' kinds</div>';
			str += '<div class="ui-state-highlight ui-corner-all" style="padding: 5px;">' + $landscape.flora + ' kinds</div>';
			str += '</div>';
		}
		$('#landscape-listing').html(str);
		$('#landscape-listing').accordion(
		{
			collapsible: true,
			active : false,
			heightStyle : 'content'
		});
	},
	
	setSpinner : function()
	{
		$('#landscape-spinner').spinner(
		{
			min : 0,
			max : 18,
			stop : function( event, ui )
			{
				myMap.map.setZoom( parseInt( $(this).val(), 10 ));
			}
		});
	},
	
	setSlider : function()
	{
		$('#landscape-slider').slider(
		{
			min : myMap.minVisitor,
			max : myMap.maxVisitor,
			range : true,
			values : [myMap.minVisitor, myMap.maxVisitor],
			step : 100,
			slide : function(event, ui)
			{
				$('#landscape-currentRange').text(ui.values[0] + ' - ' + ui.values[1]);
			},
			stop : function(event, ui)
			{
				$('#landscape-listing h3').each(function()
				{
					var visitor = parseInt($(this).data('visitor'), 10);
					//headerIndex corresponds to 0 based index of landscapes in object as well as in DOM
					var headerIndex = $('#landscape-listing h3').index($(this));
					if(visitor >= ui.values[0] && visitor <= ui.values[1])
					{
						$('#landscape-listing h3:eq('+headerIndex+')').show();
						myMap.markers[headerIndex].setMap(myMap.map);
					}
					else
					{
						$('#landscape-listing h3:eq('+ headerIndex +')').hide();
						$('#landscape-listing div.ui-accordion-content:eq('+ headerIndex +')').hide();
						myMap.markers[headerIndex].setMap(null);
					}
				});
			},
		});
	},
	setIcon : function() {
		var icon = [
			'img/icon-coral.png',
			'img/icon-salmon.png',
			'img/icon-taipei-101.png',
			'img/icon-deer.png',
			'img/icon-mullet-roe.png',
			'img/icon-pineapple.png',
			];
		var iconTitle = [
			'Green Island - The oldest and largest living coral head in the world.',
			'Formosan landlocked salmon - One of the rarest fish in the world.',
			'Taipei 101 - A landmark supertall skyscraper in Xinyi District.',
			'Formosan sika deer - A subspecies of sika deer endemic to the island of Taiwan.',
			'Mullet Roe - Demand from Mainland China hugely boosts price of Taiwanese mullet roe.',
			'Pineapple cake - The quintessential gift and treat of Taiwan.',
			];
		var iconLatLng = [
			{lat: 22.663604, lng: 121.489952},
			{lat: 24.369747, lng: 121.312153},
			{lat: 25.034255, lng: 121.564453},
			{lat: 21.974698, lng: 120.754585},
			{lat: 22.771212, lng: 120.109774},
			{lat: 23.933973, lng: 120.638040},
			];
		for(var i = 0; i < icon.length; i++) {
		  var $marker = new google.maps.Marker({
              position: iconLatLng[i],
              map: this.map,
              icon: icon[i],
              title: iconTitle[i], 
		  });
		}
	}
};