$(document).ready(function()
{
	$('.scroll-down a').on('click', function(e) {
	  e.preventDefault();
	  scrollTo = this.id;
	    $('html, body').animate({
	      scrollTop: $("form#" + scrollTo).offset().top
		}, 3000); 
	});
	
	$( "#front-end" ).hover(
      function() {
      	$( this ).addClass("bg-danger");
      }, function() {
    $( this ).removeClass("bg-danger");
      }
    );
    
    $( "#back-end" ).hover(
      function() {
      	$( this ).addClass("bg-danger");
      }, function() {
    $( this ).removeClass("bg-danger");
      }
    );
    
    $( "#design" ).hover(
      function() {
      	$( this ).addClass("bg-danger");
      }, function() {
    $( this ).removeClass("bg-danger");
      }
    );
    
    $( "#collapse-2" ).hover(
      function() {
      	$( this ).addClass("bg-danger");
      }, function() {
    $( this ).removeClass("bg-danger");
      }
    );
    
    $( "#collapse-3" ).hover(
      function() {
      	$( this ).addClass("bg-danger");
      }, function() {
    $( this ).removeClass("bg-danger");
      }
    );
	
	$('#front-end').tooltip(
	{
		items : '#front-end',
		position: { my: 'left top', at: 'bottom', collision : 'flip' },
		content : function()
		{
			var strFrontEnd = '<strong>1 - Front End</strong><br>I am an front-end engineer who write most codes in <b>JavaScript</b>, <b>HTML</b>, and <b>CSS</b>, ';
			    strFrontEnd += 'having experiences building real world web apps, and also proficient in many JavaScript libraries, '; 
			    strFrontEnd += 'like <b>jQuery</b>, <b>jQuery UI</b>, and <b>Bootstrap</b>. <br>My interests mainly lie in <b>making dynamic websites</b>, ';
			    strFrontEnd += 'such as animation with JavaScript and CSS, and <b>interactive user interface</b>, such as personalization options. ';
			    return strFrontEnd;
		}
	});
	
	$('#back-end').tooltip(
	{
		items : '#back-end',
		position: { my: 'left top', at: 'bottom', collision : 'flip' },
		content : function()
		{
			var strFrontEnd = '<strong>2 - Back End</strong><br>My goal is becoming a <b>full-stack web developer</b>. It is definitely an embarassing moment when you told someone you are ';
			    strFrontEnd += 'an outstanding web developer but you don&apos;t have a solid, well-functioning work. <br>So, apart from front-end techniques, '; 
			    strFrontEnd += 'I also learn back-end development skills and use them practically. For example, I built this site based on <b>node.js</b>, ';
			    strFrontEnd += '<b>Express.js</b>, and <b>MongoDB</b>. Before, I&apos;ve built some web apps with <b>ruby on rails</b>. ';
			    return strFrontEnd;
		}
	});
	
	$('#design').tooltip(
	{
		items : '#design',
		position: { my: 'left top', at: 'bottom', collision : 'flip' },
		content : function() {
			var strDesign = '<strong>3 - Design</strong><br>When I design my websites, my guide is <b>KISS principle (keep it simple, stupid)</b>. Simple, does not mean no design, indicates ';
			    strDesign += 'letting users understand simply by intuition even without thinking. And this is the reason I prefer <b>interactive user interface</b>.';
			    return strDesign;
		}
	});
	
	$('#collapse-2').tooltip(
	{
		items : '#collapse-2',
		position: { my: 'left top', at: 'bottom', collision : 'flip' },
		content : function() {
		var strProject = '<strong>4 - Show more, tell less</strong><br>You can see my codes at <b>Github</b> by clicking the titles of my projects. Other than this website and web apps here, I&apos;ve built a website ';
			strProject += 'for a land scrivener service company in Taiwan, and a side project showing data with Google Maps in a silicon valley hackathon.';
			return strProject;
		},
	});
	
	$('#quantum-computing').tooltip(
	{
		items : '#quantum-computing',
		position: { my: 'left top', at: 'bottom', collision : 'flip' },
		content : function() { 
			var strQuantumComputing = '<strong>5 - Master Thesis</strong><br>I love cutting-edge technologies. When I was still a graduate student, I went to maker faire both in California and New York, ';
			    strQuantumComputing += 'participated in hackathon, joined events held by Y Combinator. <br>Now I found it is difficult to find a job making ';
			    strQuantumComputing += 'quantum computers, but in my research process, I <b>took computational courses</b>, <b>dealt with tons of data by programming</b>, and ';
			    strQuantumComputing += '<b>studying algorithm (even quantum algorithm)</b>. It&apos;s been <b>10 years</b> passed since I learned C++ when I was a child. ';
			    return strQuantumComputing;
		},
	});
	
	$('#collapse-3').tooltip(
	{
		items : '#collapse-3',
		position: { my: 'left top', at: 'bottom', collision : 'flip' },
		content : function() { 
		var strEducation = '<strong>6 - Education</strong><br>People major in physics are usually well-trained with <b>conceptual, logical thinking</b>, and get accustomed to <b>quantitative methods</b>. ';
			strEducation += '<br>In my opinion, web development is another approach to deal with data. Front-end works for accessing and displaying data; back-end ';
			strEducation += 'works for managing and modeling data. Too many works physicists do cannot be understood by the public, but front-end is a way ';
			strEducation += 'to enable data to communicate with people.';
			return strEducation;
		},
	});
	
	$('#contact').tooltip(
	{
		items : '#contact',
		content : function()
		{
			var strContact = '<img src="http://maps.googleapis.com/maps/api/staticmap?center=Mountain+View,+California&zoom=13&scale=false&size=280x200&maptype=roadmap&format=png&visual_refresh=true"/>';
				strContact+= '<br/>Now I live in Mountain View, California.';
				strContact+= '<br><span class="ui-icon ui-icon-home" style="float: left; margin-right: 5px;"></span> Mountain View, California';
				strContact+= '<br><span class="ui-icon ui-icon-signal-diag" style="float: left; margin-right: 5px;"></span> 1-3474219768';
				strContact+= '<br><span class="ui-icon ui-icon-mail-closed" style="float: left; margin-right: 5px;"></span> seraph317@gmail.com';
				strContact+= "<br>Feel free to contact me to discuss how I can contribute to your company.";
				return strContact;
		}
	});
	
	$('#startTour').tooltip(
	{
		items : '#startTour',
		content : function()
		{
			var strStartTour = 'May I introduce myself?';
			    strStartTour += '<br>Click and take a tour.';
			return strStartTour;
		}
	});

	tour.init();
});

var tourDialog = $('#dialog').dialog(
{
	autoOpen : false,
	minWidth : 315,
	draggable : false,
	buttons: [
	{
	  id : 'buttonPrevious',	
	  text: 'Previous',
	  click: function() 
	  {
		tour.navigate('previous');
	  },
	  icons: 
	  { 
		primary: 'ui-icon ui-icon-carat-1-w'
	  }
	},
	{
	  id : 'buttonNext',	
	  text: 'Next',
	  click: function(event) 
	  {
		tour.navigate('next');
	  },
	  icons: 
	  { 
		secondary: 'ui-icon ui-icon-carat-1-e'
	  }
    },
	{
	  text: 'End Tour',
	  click: function() 
	  {
		tour.endTour();
	  },
	  icons: 
	  { 
		secondary: 'ui-icon ui-icon-close'
	  }
	}
  ],
  show : 'fold',
  hide : 'fold'
});

var steps = 
	[
		{
			element : '#collapse-1',
			title : 'Recruitable',
			content : function() {
				var strRecruitable = 'Hello, my name is <b>Yung-Shun Chang</b>. I just graduated from NYU getting my master degree, and I am pursuing my ';
				    strRecruitable += 'career as a <b>front-end web developer</b> now. <br>Very glad to talk with you.';
				    return strRecruitable;
			},
			isAccordion : true,
			accordionIndex : 1,
			sequence : 1
		},
		{
			element : '#front-end',
			title : 'What I do?',
			content : function() {
				var strFrontEnd = 'I am an front-end engineer who write most codes in <b>JavaScript</b>, <b>HTML</b>, and <b>CSS</b>, ';
			    strFrontEnd += 'having experiences building real world web apps, and also proficient in many JavaScript libraries, '; 
			    strFrontEnd += 'like <b>jQuery</b>, <b>jQuery UI</b>, and <b>Bootstrap</b>. <br>My interests mainly lie in <b>making dynamic websites</b>, ';
			    strFrontEnd += 'such as animation with JavaScript and CSS, and <b>interactive user interface</b>, such as personalization options. ';
			    return strFrontEnd;
			},
			isAccordion : false,
			sequence : 2
		},
		{
			element : '#back-end',
			title : 'Be a full stack web developer!',
			content : function() {
			var strFrontEnd = 'My goal is becoming a <b>full stack web developer</b>. It is definitely an embarassing moment when you told someone you are ';
			    strFrontEnd += 'an outstanding web developer but you don&apos;t have a solid, well-functioning work. <br>So, apart from front-end techniques, '; 
			    strFrontEnd += 'I also learn back-end development skills and use them practically. For example, I built this site based on <b>node.js</b>, ';
			    strFrontEnd += '<b>Express.js</b>, and <b>MongoDB</b>. Before, I&apos;ve built some web apps with <b>ruby on rails</b>. ';
			    return strFrontEnd;
			},
			isAccordion : false,
			sequence : 3
		},
		{
			element : '#design',
			title : 'KISS principle',
			content : function() {
			var strDesign = 'When I design my websites, my guide is <b>KISS principle (keep it simple, stupid)</b>. Simple, does not mean no design, indicates ';
			    strDesign += 'letting users understand simply by intuition even without thinking. And this is the reason I prefer <b>interactive user interface</b>.';
			    return strDesign;
			},
			isAccordion : false,
			sequence : 4
		},
		{
			element : '#collapse-2',
			title : 'Real world web app experiences',
			content : function() {
			var strProject = 'You can see my codes at <b>Github</b> by clicking the titles of my projects. Other than this website and web apps here, I&apos;ve built a website ';
			    strProject += 'for a land scrivener service company in Taiwan, and a side project showing data with Google Maps in a silicon valley hackathon.';
			    return strProject;
			},
			isAccordion : true,
			accordionIndex : 2,
			sequence : 5
		},
		{
			element : '#quantum-computing',
			title : "What's this?",
			content : function() { 
			var strQuantumComputing = 'I love cutting-edge technologies. When I was still a graduate student, I went to maker faire both in California and New York, ';
			    strQuantumComputing += 'participated in hackathon, joined events held by Y Combinator. <br>Now I found it is difficult to find a job making ';
			    strQuantumComputing += 'quantum computers, but in my research process, I <b>took computational courses</b>, <b>dealt with tons of data by programming</b>, and ';
			    strQuantumComputing += '<b>studying algorithm (even quantum algorithm)</b>. It&apos;s been <b>10 years</b> passed since I learned C++ when I was a child. ';
			    return strQuantumComputing;
			},
			isAccordion : false,
			sequence : 6
		},
		{
			element : '#collapse-3',
			title : 'Why not data scientist?',
			content : function() { 
			var strEducation = 'People major in physics are usually well-trained with <b>conceptual, logical thinking</b>, and get accustomed to <b>quantitative methods</b>. ';
			    strEducation += '<br>In my opinion, web development is another approach to deal with data. Front-end works for accessing and displaying data; back-end ';
			    strEducation += 'works for managing and modeling data. Too many works physicists do cannot be understood by the public, but front-end is a way ';
			    strEducation += 'to enable data to communicate with people.';
			    return strEducation;
			},
			isAccordion : true,
			accordionIndex : 3,
			sequence : 7
		},
		{
			element : '#contact',
			title : 'Contact Me',
			content : function()
			{
				var strContact = '<img src="http://maps.googleapis.com/maps/api/staticmap?center=Mountain+View,+California&zoom=13&scale=false&size=280x200&maptype=roadmap&format=png&visual_refresh=true"/>';
				    strContact+= '<br/>Now I live in Mountain View, California.';
			     	strContact+= '<br><span class="ui-icon ui-icon-home" style="float: left; margin-right: 5px;"></span> Mountain View, California';
			     	strContact+= '<br><span class="ui-icon ui-icon-signal-diag" style="float: left; margin-right: 5px;"></span> 1-3474219768';
				    strContact+= '<br><span class="ui-icon ui-icon-mail-closed" style="float: left; margin-right: 5px;"></span> seraph317@gmail.com';
				    strContact+= "<br>Feel free to contact me to discuss how I can contribute to your company.";
				return strContact;
			},
			sequence : 8
		},
		{
			element : '#startTour',
			title : 'Thank You!',
			content : function()
			{
				var strThank = 'Thank you very much for going through the tour. If you have interest in letting me working with you or just making friend, please contact me. ';
			        strThank += '<br>A new friend is always welcome.';
			    return strThank;
			},
			sequence : 9
		}
	];

var tour = 
	{
		triggerElement : '#startTour',
		tourStep : 0, 
		tourSteps : steps,
		defaultTitle : 'Welcome to the tour !',
		defaultContent: 'In this tour, I will give a brief self introduction. <br> Please use next previous buttons to proceed. Click the End Tour button whenever you want to finish the tour.',
		init : function()
		{
			if(this.tourSteps == undefined || this.tourSteps.length == 0)
			{
				alert('Cannot start tour');
				return;
			}
			
			$(this.triggerElement).on('click', function(event)
			{
				tour.showStep(tour.defaultTitle, tour.defaultContent, $(this));
				return false;
			});
		},
		showStep : function(tourStepTitle, tourStepContent, whichElement)
		{
			$('*').tooltip({ disabled: true });
			this.prevNextButtons();
			$('body').animate(
			{
				scrollTop: $(whichElement).offset().top
			}, 500, function()
				{
					$('.bg-danger').removeClass('bg-danger');
						
					$(whichElement).addClass('bg-danger');
					
					tourDialog.dialog('option', 'title', tourStepTitle);

					tourDialog.html(tourStepContent);

					tourDialog.dialog('option', 'position', { my: 'left top', at: 'bottom', of: whichElement, collision : 'flip' });
					
					tourDialog.dialog('open');
				});
		},
		prevNextButtons : function()
		{
			$('#buttonNext').button('enable');
			$('#buttonPrevious').button('enable');
			if(this.tourStep == 0 || this.tourStep == 1)
			{
				$('#buttonPrevious').button('disable');
			}
			if(this.tourStep == this.tourSteps.length)
			{
				$('#buttonNext').button('disable');
			}
			return;
		},
		navigate : function(previousOrNext)
		{
			if(previousOrNext == 'previous')
			{
				(this.tourStep) = (this.tourStep) - 1;
			}
			else 
			{
				this.tourStep = this.tourStep + 1;
			}
			
			for(var i = 0; i<this.tourSteps.length; i++)
			{
				if(this.tourSteps[i].sequence == this.tourStep)
				{
					if(this.tourSteps[i].isAccordion)
					{
					    $(this.tourSteps[i].element).collapse('show'); 
					}
					this.showStep(this.tourSteps[i].title, this.tourSteps[i].content, this.tourSteps[i].element);
					return;
				}
			}
		},
		endTour : function()
		{
			this.tourStep = 0;
			$('.bg-danger').removeClass('bg-danger');
			tourDialog.dialog( 'close' );
			$('*').tooltip({ disabled: false });
		}
	};