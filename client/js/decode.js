$(document).ready(function()
{  
    var obj;
	
    function htmlDecode(input){
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    }
    
    function displayArticle(){
        $.getJSON('ajax/articles.json')
        .done(function(data) {
            obj = data;
            for(var i = 0; i < obj.length; i++){
                $('#article').after("<div class='col-md-8' id='"+i+"'><div class='panel panel-default'><div class='panel-body'><h2 style='margin-top: 0px; margin-bottom: 0px;'><span class='glyphicon glyphicon-file fi-page-edit'></span> "+obj[i].title+"</h2><br><p style='margin: auto 25px auto 25px;'>"+htmlDecode(obj[i].text)+"</p></div></div></div>").text();
                $('#list').after("<h4 style='margin-bottom: 10px;'><a href='#"+i+"' style='margin-left: 20px;'>"+obj[i].title+"</a></h4>").text();
            }
        }).fail(function(){
            $('#article').html('sorry'); 
        });
    }
    
    displayArticle();
});