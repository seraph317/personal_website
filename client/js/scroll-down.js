$(document).ready(function() {
	var menuHeight = $("#menu").height();
	var getMenu = document.getElementById("menu");
	var offsetMenu = getMenu.offsetTop;
	var getColumn = document.getElementById("column-1");
	var offsetColumn = getColumn.offsetTop;
	var offset = offsetColumn - offsetMenu;

	$('.scroll-down a').on('click', function(e) {
		e.preventDefault();
	    scrollTo = this.id;
	   	if ($("#menu").hasClass("navbar-fixed-top")) {
	   		    $('html, body').animate({
	            scrollTop: $("div#" + scrollTo).offset().top - menuHeight
		    }, 5000); 
		} else {
		    $('html, body').animate({
		        scrollTop: $("div#" + scrollTo).offset().top - menuHeight - offset
		    }, 5000);
		}
	});
});